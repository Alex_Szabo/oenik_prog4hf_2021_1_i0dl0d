﻿// <copyright file="StudentLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrivingSchool.UI.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DrivingSchool.Program;
    using DrivingSchool.UI.Data;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// Logic class for student methods.
    /// </summary>
    internal class StudentLogic : IStudentLogic
    {
        private IMessenger messengerService;
        private IEditorService editorService;
        private Factory factory;

        /// <summary>
        /// Initializes a new instance of the <see cref="StudentLogic"/> class.
        /// </summary>
        /// <param name="messengerService">A messenger interface.</param>
        /// <param name="editorService">An IEditorService interface.</param>
        /// <param name="factory">Factory of the layers below UI.</param>
        public StudentLogic(IMessenger messengerService, IEditorService editorService, Factory factory)
        {
            this.messengerService = messengerService;
            this.editorService = editorService;
            this.factory = factory;
        }

        /// <summary>
        /// Adds a new student to students.
        /// </summary>
        /// <param name="students">List of all students.</param>
        public void AddStudent(IList<Student> students)
        {
            Student newStudent = new Student();
            if (this.editorService.EditStudent(newStudent) == true)
            {
                bool emptypropertycheck = newStudent.GetType().GetProperties()
                     .Where(prop => prop.PropertyType == typeof(string))
                     .Select(prop => (string)prop.GetValue(newStudent))
                     .Any(value => string.IsNullOrEmpty(value));
                if (emptypropertycheck == false)
                {
                    this.factory.StudentsDrivingLogic.AddStudent(newStudent.FirstName, newStudent.LastName, newStudent.DateOfBirth, newStudent.Email, newStudent.PhoneNumber, newStudent.Sex);
                    var q = this.factory.StudentsDrivingLogic.GetAllStudents().Last();
                    students.Add(new Student() { FirstName = q.FirstName, LastName = q.LastName, Email = q.Email, DateOfBirth = q.DateOfBirth.ToShortDateString(), PhoneNumber = q.PhoneNumber, Sex = q.Sex, Id = q.StudentId });
                    this.messengerService.Send("Student added!", "LogicAction");
                }
                else
                {
                    this.messengerService.Send("All field must be not empty!", "LogicAction");
                }
            }
            else
            {
                this.messengerService.Send("Add cancelled!", "LogicAction");
            }
        }

        /// <summary>
        /// Modifies a student.
        /// </summary>
        /// <param name="editStud">A student to modify.</param>
        public void EditStudent(Student editStud)
        {
            if (editStud == null)
            {
                this.messengerService.Send("Edit failed!", "LogicAction");
                return;
            }

            Student clone = new Student();
            clone.CopyFrom(editStud);
            if (this.editorService.EditStudent(clone) == true)
            {
                bool emptypropertycheck = clone.GetType().GetProperties()
                   .Where(prop => prop.PropertyType == typeof(string))
                   .Select(prop => (string)prop.GetValue(clone))
                   .Any(value => string.IsNullOrEmpty(value));
                if (emptypropertycheck == false)
                {
                    if (editStud.Email != clone.Email)
                    {
                        this.factory.StudentsDrivingLogic.ChangeEmail(editStud.Id, clone.Email);
                    }

                    if (editStud.FirstName != clone.FirstName)
                    {
                        this.factory.StudentsDrivingLogic.ChangeFirstName(editStud.Id, clone.FirstName);
                    }

                    if (editStud.LastName != clone.LastName)
                    {
                        this.factory.StudentsDrivingLogic.ChangeLastName(editStud.Id, clone.LastName);
                    }

                    if (editStud.PhoneNumber != clone.PhoneNumber)
                    {
                        this.factory.StudentsDrivingLogic.ChangePhoneNumber(editStud.Id, clone.PhoneNumber);
                    }

                    if (editStud.Sex != clone.Sex)
                    {
                        this.factory.StudentsDrivingLogic.ChangeSex(editStud.Id, clone.Sex);
                    }

                    if (editStud.DateOfBirth != clone.DateOfBirth)
                    {
                        this.factory.StudentsDrivingLogic.ChangeDOB(editStud.Id, clone.DateOfBirth);
                        clone.DateOfBirth = clone.DateOfBirth.Split('/')[2].Split(" ")[0] + ". " + clone.DateOfBirth.Split('/')[0] + ". " + clone.DateOfBirth.Split('/')[1] + ".";
                        DateTime dateTime = DateTime.ParseExact(clone.DateOfBirth, "yyyy. M. d.", null);
                        clone.DateOfBirth = dateTime.ToShortDateString();
                    }
                    else
                    {
                        editStud.DateOfBirth = clone.DateOfBirth;
                    }

                    editStud.CopyFrom(clone);
                    this.messengerService.Send("Updated!", "LogicAction");
                }
                else
                {
                    this.messengerService.Send("All field must be not empty!", "LogicAction");
                }
            }
        }

        /// <summary>
        /// Removes a student from students.
        /// </summary>
        /// <param name="students">List of all students.</param>
        /// <param name="student">The student we want to remove.</param>
        public void RemoveStudent(IList<Student> students, Student student)
        {
            if (student != null && students.Remove(student))
            {
                this.factory.StudentsDrivingLogic.RemoveStudent(student.Id);
                this.messengerService.Send("Student removed!", "LogicAction");
            }
            else
            {
                this.messengerService.Send("Student remove failed!", "LogicAction");
            }
        }

        /// <summary>
        /// Gets all students.
        /// </summary>
        /// <returns>List of all students.</returns>
        public IList<Student> GetAllStudent()
        {
            var q = this.factory.StudentsDrivingLogic.GetAllStudents();
            IList<Student> allstudents = new List<Student>();
            foreach (var item in q)
            {
                allstudents.Add(new Student() { Id = item.StudentId, FirstName = item.FirstName, LastName = item.LastName, DateOfBirth = item.DateOfBirth.ToShortDateString(), Email = item.Email, PhoneNumber = item.PhoneNumber, Sex = item.Sex });
            }

            return allstudents;
        }
    }
}
