﻿// <copyright file="IStudentLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrivingSchool.UI.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DrivingSchool.UI.Data;

    /// <summary>
    /// Interface for student methods.
    /// </summary>
    internal interface IStudentLogic
    {
        /// <summary>
        /// Adds a new student to students.
        /// </summary>
        /// <param name="students">List of students.</param>
        void AddStudent(IList<Student> students);

        /// <summary>
        /// Removes a student from students.
        /// </summary>
        /// <param name="students">List of students.</param>
        /// <param name="student">The student we want to remove.</param>
        void RemoveStudent(IList<Student> students, Student student);

        /// <summary>
        /// Modifies a student.
        /// </summary>
        /// <param name="editStud">A student to modify.</param>
        void EditStudent(Student editStud);

        /// <summary>
        /// Gets all students.
        /// </summary>
        /// <returns>A list of all students.</returns>
        IList<Student> GetAllStudent();
    }
}
