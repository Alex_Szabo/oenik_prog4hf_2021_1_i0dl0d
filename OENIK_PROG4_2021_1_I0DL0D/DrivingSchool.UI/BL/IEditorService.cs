﻿// <copyright file="IEditorService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrivingSchool.UI.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DrivingSchool.UI.Data;

    /// <summary>
    /// Interface to modify student.
    /// </summary>
    internal interface IEditorService
    {
        /// <summary>
        /// Checks if the object is a valid student.
        /// </summary>
        /// <param name="st">A student object.</param>
        /// <returns>True or false.</returns>
        bool EditStudent(Student st);
    }
}
