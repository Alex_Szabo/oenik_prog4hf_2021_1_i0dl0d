﻿// <copyright file="EditorViaWindow.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrivingSchool.UI.UI
{
    using DrivingSchool.UI.BL;
    using DrivingSchool.UI.Data;

    /// <summary>
    /// Edit window.
    /// </summary>
    internal class EditorViaWindow : IEditorService
    {
        /// <summary>
        /// Edit window for student.
        /// </summary>
        /// <param name="st">A student.</param>
        /// <returns>Done or cancelled.</returns>
        public bool EditStudent(Student st)
        {
            EditorWindow win = new EditorWindow(st);
            return win.ShowDialog() ?? false;
        }
    }
}
