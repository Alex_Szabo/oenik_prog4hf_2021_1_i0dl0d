﻿// <copyright file="EditorViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrivingSchool.UI.VM
{
    using DrivingSchool.UI.Data;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// ViewModel for editing.
    /// </summary>
    internal class EditorViewModel : ViewModelBase
    {
        private Student student;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorViewModel"/> class.
        /// </summary>
        public EditorViewModel()
        {
            this.student = new Student();
            if (this.IsInDesignMode)
            {
                this.student.FirstName = "Alex";
                this.student.LastName = "Szabó";
                this.student.DateOfBirth = "1988. 11. 24";
                this.student.Email = "alexszabo@gmail.com";
                this.student.PhoneNumber = "0036301234567";
                this.student.Sex = "male";
            }
        }

        /// <summary>
        /// Gets or sets a student.
        /// </summary>
        public Student Student
        {
            get { return this.student; }
            set { this.Set(ref this.student, value); }
        }
    }
}
