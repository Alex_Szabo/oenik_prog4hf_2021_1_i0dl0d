﻿// <copyright file="MainViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrivingSchool.UI.VM
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using CommonServiceLocator;
    using DrivingSchool.UI.BL;
    using DrivingSchool.UI.Data;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// ViewModel for MainWindow.
    /// </summary>
    internal class MainViewModel : ViewModelBase
    {
        private IStudentLogic studentLogic;

        private Student studentSelected;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        /// <param name="studentLogic">Student logic.</param>
        public MainViewModel(IStudentLogic studentLogic)
        {
            this.studentLogic = studentLogic;

            this.AddCmd = new RelayCommand(() => this.studentLogic.AddStudent(this.Students));
            this.RemoveCmd = new RelayCommand(() => this.studentLogic.RemoveStudent(this.Students, this.StudentSelected));
            this.EditCmd = new RelayCommand(() => this.studentLogic.EditStudent(this.StudentSelected));

            this.Students = new ObservableCollection<Student>();

            if (this.IsInDesignMode)
            {
                Student s1 = new Student() { FirstName = "János", LastName = "Szabó", DateOfBirth = "1964. 08. 06", Email = "janika@gmail.com", PhoneNumber = "0036701964086", Sex = "male" };
                this.Students.Add(s1);
            }
            else
            {
                IList<Student> studentsfromdb = this.studentLogic.GetAllStudent();
                foreach (var item in studentsfromdb)
                {
                    this.Students.Add(item);
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        public MainViewModel()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IStudentLogic>())
        {
        }

        /// <summary>
        /// Gets or sets the selected student.
        /// </summary>
        public Student StudentSelected
        {
            get { return this.studentSelected; }
            set { this.Set(ref this.studentSelected, value); }
        }

        /// <summary>
        /// Gets ObservableCollection of students.
        /// </summary>
        public ObservableCollection<Student> Students { get; private set; }

        /// <summary>
        /// Gets add command.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets remove command.
        /// </summary>
        public ICommand RemoveCmd { get; private set; }

        /// <summary>
        /// Gets edit command.
        /// </summary>
        public ICommand EditCmd { get; private set; }
    }
}
