﻿// <copyright file="VehicleUsageLogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrivingSchool.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Driving.School.Repository;
    using DrivingSchool.Data.Model;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Tests for VehicleUsageLogic logic.
    /// </summary>
    [TestFixture]
    public class VehicleUsageLogicTest
    {
        /// <summary>
        /// Tests Remove method in VehicleUsageLogic.
        /// </summary>
        [Test]
        public void TestDeletingACar()
        {
            Mock<ICarRepository> mockedCarRepo = new Mock<ICarRepository>(MockBehavior.Loose);
            Mock<ILessonRepository> mockedLessonRepo = new Mock<ILessonRepository>(MockBehavior.Loose);

            mockedCarRepo.Setup(carRepo => carRepo.Remove(It.IsAny<int>())).Verifiable();

            VehicleUsageLogic logic = new VehicleUsageLogic(mockedCarRepo.Object, mockedLessonRepo.Object);

            logic.RemoveCar(22);

            mockedCarRepo.Verify(carRepo => carRepo.Remove(22), Times.Once);
        }

        /// <summary>
        /// Tests TotalDistances method in VehicleUsageLogic.
        /// </summary>
        [Test]
        public void TotalDistanceOfCars()
        {
            Mock<ICarRepository> mockedCarRepo = new Mock<ICarRepository>(MockBehavior.Loose);
            Mock<ILessonRepository> mockedLessonRepo = new Mock<ILessonRepository>(MockBehavior.Loose);

            VehicleUsageLogic logic = new VehicleUsageLogic(mockedCarRepo.Object, mockedLessonRepo.Object);
            Car i0 = new Car() { CarId = 1 };
            Car i1 = new Car() { CarId = 2 };
            List<Car> cars = new List<Car>() { i0, i1 };
            Lesson l0 = new Lesson() { CarId = 1, Distance = 55 };
            Lesson l1 = new Lesson() { CarId = 1, Distance = 75 };
            Lesson l2 = new Lesson() { CarId = 2, Distance = 35 };
            Lesson l3 = new Lesson() { CarId = 2, Distance = 45 };
            List<Lesson> lessons = new List<Lesson>() { l0, l1, l2, l3 };
            mockedCarRepo.Setup(carRepo => carRepo.GetAll()).Returns(cars.AsQueryable());
            mockedLessonRepo.Setup(lessonRepo => lessonRepo.GetAll()).Returns(lessons.AsQueryable());

            var q = logic.TotalDistances();

            TotalDistances t1 = new TotalDistances() { CarId = 1, TotalDistance = 130 };
            TotalDistances t2 = new TotalDistances() { CarId = 2, TotalDistance = 80 };
            List<TotalDistances> expectedResult = new List<TotalDistances>() { t1, t2 };

            Assert.That(q, Is.EquivalentTo(expectedResult));
        }
    }
}
