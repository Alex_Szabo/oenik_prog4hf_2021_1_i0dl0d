﻿// <copyright file="StudentsDrivingLogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrivingSchool.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Driving.School.Repository;
    using DrivingSchool.Data.Model;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Tests for StudentsDrivingLogic logic.
    /// </summary>
    [TestFixture]
    public class StudentsDrivingLogicTest
    {
        /// <summary>
        /// Tests GetAllStudents method in StudentsDrivingLogic.
        /// </summary>
        [Test]
        public void GetAllStudentsTest()
        {
            Mock<IStudentRepository> mockedStudentRepo = new Mock<IStudentRepository>(MockBehavior.Loose);
            Mock<ILessonRepository> mockedLessonRepo = new Mock<ILessonRepository>(MockBehavior.Loose);

            List<Student> students = new List<Student>()
            {
                new Student() { FirstName = "Alex", LastName = "Kovács", PhoneNumber = "303981987" },
                new Student() { FirstName = "Virág", LastName = "Hajnal", Email = "hvirag@gmail.com" },
                new Student() { FirstName = "Júlia", LastName = "Szép", Sex = "female" },
            };
            mockedStudentRepo.Setup(studRepo => studRepo.GetAll()).Returns(students.AsQueryable());

            StudentsDrivingLogic logic = new StudentsDrivingLogic(mockedStudentRepo.Object, mockedLessonRepo.Object);

            var allstudents = logic.GetAllStudents();

            mockedStudentRepo.Verify(mockedStudentRepo => mockedStudentRepo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Tests AverageSpeedByeSex method in StudentsDrivingLogic.
        /// </summary>
        [Test]
        public void AverageSpeedBySexTest()
        {
            Mock<IStudentRepository> mockedStudentRepo = new Mock<IStudentRepository>(MockBehavior.Loose);
            Mock<ILessonRepository> mockedLessonRepo = new Mock<ILessonRepository>(MockBehavior.Loose);

            List<Student> students = new List<Student>()
            {
                new Student() { FirstName = "Alex", LastName = "Kovács", Sex = "male", PhoneNumber = "303981987", StudentId = 1 },
                new Student() { FirstName = "Virág", LastName = "Hajnal", Sex = "female", Email = "hvirag@gmail.com", StudentId = 2 },
                new Student() { FirstName = "Júlia", LastName = "Szép", Sex = "female", StudentId = 3 },
            };

            List<Lesson> lessons = new List<Lesson>()
            {
                new Lesson() { StudentId = 1, Distance = 50, LessonTime = 60 },
                new Lesson() { StudentId = 2, Distance = 30, LessonTime = 60 },
                new Lesson() { StudentId = 3, Distance = 50, LessonTime = 60 },
                new Lesson() { StudentId = 1, Distance = 100, LessonTime = 60 },
            };
            mockedStudentRepo.Setup(studRepo => studRepo.GetAll()).Returns(students.AsQueryable());
            mockedLessonRepo.Setup(lessonRepo => lessonRepo.GetAll()).Returns(lessons.AsQueryable());

            StudentsDrivingLogic logic = new StudentsDrivingLogic(mockedStudentRepo.Object, mockedLessonRepo.Object);
            List<AverageSpeedBySex> expectedresult = new List<AverageSpeedBySex>()
            {
                new AverageSpeedBySex() { Sex = "male", AverageSpeed = 75 },
                new AverageSpeedBySex() { Sex = "female", AverageSpeed = 40 },
            };

            var avgspeedbysex = logic.AverageSpeedBySex();

            Assert.That(expectedresult, Is.EquivalentTo(avgspeedbysex));
        }

        /// <summary>
        /// Tests GetOne method in StudentsDrivingLogic.
        /// </summary>
        [Test]
        public void GetOneStudentsData()
        {
            Mock<IStudentRepository> mockedStudentRepo = new Mock<IStudentRepository>(MockBehavior.Loose);
            Mock<ILessonRepository> mockedLessonRepo = new Mock<ILessonRepository>(MockBehavior.Loose);

            List<Student> students = new List<Student>()
            {
                new Student() { FirstName = "Alex", LastName = "Kovács", PhoneNumber = "303981987" },
                new Student() { FirstName = "Virág", LastName = "Hajnal", Email = "hvirag@gmail.com" },
                new Student() { FirstName = "Júlia", LastName = "Szép", Sex = "female" },
            };

            mockedStudentRepo.Setup(studRepo => studRepo.GetOne(2)).Verifiable();

            StudentsDrivingLogic logic = new StudentsDrivingLogic(mockedStudentRepo.Object, mockedLessonRepo.Object);

            logic.GetStudentId(2);

            mockedStudentRepo.Verify(studRepo => studRepo.GetOne(2), Times.Once);
        }
    }
}
