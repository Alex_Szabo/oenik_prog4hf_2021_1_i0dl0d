﻿// <copyright file="StaffLogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrivingSchool.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Driving.School.Repository;
    using DrivingSchool.Data.Model;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Tests for StaffLogic logic.
    /// </summary>
    [TestFixture]
    public class StaffLogicTest
    {
        /// <summary>
        /// Tests AddNewInstructor method in StaffLogic.
        /// </summary>
        [Test]
        public void TestAddNewInstructor()
        {
            Mock<IInstructorRepository> mockedInstructorRepo = new Mock<IInstructorRepository>(MockBehavior.Loose);
            Mock<ILessonRepository> mockedLessonRepo = new Mock<ILessonRepository>(MockBehavior.Loose);

            mockedInstructorRepo.Setup(instructorRepo => instructorRepo.Add(It.IsAny<Instructor>())).Verifiable();

            StaffLogic logic = new StaffLogic(mockedInstructorRepo.Object, mockedLessonRepo.Object);
            string firstName = "János";
            string lastName = "Szabó";
            string dOB = "1964.08.06.";
            string dOJ = "2020.07.12.";
            int price = 8000;
            string sex = "male";
            string phonenumber = "306548817";
            Instructor itest = new Instructor() { FirstName = firstName, LastName = lastName, DateOfBirth = DateTime.ParseExact(dOB, "yyyy.MM.dd.", null), DateOfJoin = DateTime.ParseExact(dOJ, "yyyy.MM.dd.", null), Price = price, Sex = sex, PhoneNumber = phonenumber };
            logic.AddInstructor(firstName, lastName, dOB, dOJ, price, sex, phonenumber);

            mockedInstructorRepo.Verify(mockedInstructorRepo => mockedInstructorRepo.Add(itest), Times.Once);
        }

        /// <summary>
        /// Tests ChangePrice method in StaffLogic.
        /// </summary>
        [Test]
        public void UpdatingInstructorsPrice()
        {
            Mock<IInstructorRepository> mockedInstructorRepo = new Mock<IInstructorRepository>(MockBehavior.Loose);
            Mock<ILessonRepository> mockedLessonRepo = new Mock<ILessonRepository>(MockBehavior.Loose);

            mockedInstructorRepo.Setup(instructorRepo => instructorRepo.ChangePrice(It.IsAny<int>(), It.IsAny<int>())).Verifiable();

            StaffLogic logic = new StaffLogic(mockedInstructorRepo.Object, mockedLessonRepo.Object);

            logic.ChangeInstructorPrice(1, 7200);

            mockedInstructorRepo.Verify(mockedInstructorRepo => mockedInstructorRepo.ChangePrice(1, 7200), Times.Once);
        }

        /// <summary>
        /// Tests TotalPayments method in StaffLogic.
        /// </summary>
        [Test]
        public void TotalPaymentsTest()
        {
            Mock<IInstructorRepository> mockedInstructorRepo = new Mock<IInstructorRepository>(MockBehavior.Loose);
            Mock<ILessonRepository> mockedLessonRepo = new Mock<ILessonRepository>(MockBehavior.Loose);

            List<Instructor> instructors = new List<Instructor>()
            {
                new Instructor() { InstructorId = 1, Price = 3000 },
                new Instructor() { InstructorId = 2, Price = 4000 },
                new Instructor() { InstructorId = 3, Price = 3500 },
            };

            List<Lesson> lessons = new List<Lesson>()
            {
                new Lesson() { InstructorId = 1 },
                new Lesson() { InstructorId = 2 },
                new Lesson() { InstructorId = 2 },
                new Lesson() { InstructorId = 3 },
            };

            List<TotalPayments> expectedTotalPayments = new List<TotalPayments>()
            {
                new TotalPayments() { InstructorId = 1, TotalPayment = 3000 },
                new TotalPayments() { InstructorId = 2, TotalPayment = 8000 },
                new TotalPayments() { InstructorId = 3, TotalPayment = 3500 },
            };

            mockedInstructorRepo.Setup(instructorRepo => instructorRepo.GetAll()).Returns(instructors.AsQueryable());
            mockedLessonRepo.Setup(lessonRepo => lessonRepo.GetAll()).Returns(lessons.AsQueryable());

            StaffLogic logic = new StaffLogic(mockedInstructorRepo.Object, mockedLessonRepo.Object);

            var totalPayments = logic.TotalPayments();

            Assert.That(expectedTotalPayments, Is.EquivalentTo(totalPayments));
        }
    }
}
