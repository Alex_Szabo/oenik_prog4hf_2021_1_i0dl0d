﻿// <copyright file="Student.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrivingSchool.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Student table.
    /// </summary>
    public class Student
    {
        /// <summary>
        /// Gets or sets Id of student.
        /// </summary>
        [Display(Name = "Student Id")]
        public int StudentId { get; set; }

        /// <summary>
        /// Gets or sets FirstName of student.
        /// </summary>
        [Display(Name = "Student First Name")]
        [Required]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets LastName of student.
        /// </summary>
        [Display(Name = "Student Last Name")]
        [Required]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets DateOfBirth of student.
        /// </summary>
        [Display(Name = "Student Date of Brith")]
        [Required]
        public DateTime DateOfBirth { get; set; }

        /// <summary>
        /// Gets or sets Sex of student.
        /// </summary>
        [Display(Name = "Student Sex")]
        [Required]
        public string Sex { get; set; }

        /// <summary>
        /// Gets or sets Email of student.
        /// </summary>
        [Display(Name = "Student Email")]
        [Required]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets PhoneNumber of student.
        /// </summary>
        [Display(Name = "Student Phone Number")]
        [Required]
        public string PhoneNumber { get; set; }
    }
}
