﻿// <copyright file="StudentListViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrivingSchool.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Student ViewModel.
    /// </summary>
    public class StudentListViewModel
    {
        /// <summary>
        /// Gets or sets StudentList.
        /// </summary>
        public List<Student> StudentList { get; set; }

        /// <summary>
        /// Gets or sets a student to edit.
        /// </summary>
        public Student EditedStudent { get; set; }
    }
}
