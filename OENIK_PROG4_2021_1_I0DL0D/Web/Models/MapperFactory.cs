﻿// <copyright file="MapperFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrivingSchool.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;

    /// <summary>
    /// Initializing mapper settings.
    /// </summary>
    public static class MapperFactory
    {
        /// <summary>
        /// Initializes mapper.
        /// </summary>
        /// <returns>Configured mapper.</returns>
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.Model.Student, Web.Models.Student>().
                    ForMember(dest => dest.StudentId, map => map.MapFrom(src => src.StudentId)).
                    ForMember(dest => dest.FirstName, map => map.MapFrom(src => src.FirstName)).
                    ForMember(dest => dest.LastName, map => map.MapFrom(src => src.LastName)).
                    ForMember(dest => dest.DateOfBirth, map => map.MapFrom(src => src.DateOfBirth)).
                    ForMember(dest => dest.Sex, map => map.MapFrom(src => src.Sex)).
                    ForMember(dest => dest.Email, map => map.MapFrom(src => src.Email)).
                    ForMember(dest => dest.PhoneNumber, map => map.MapFrom(src => src.PhoneNumber));
            });
            return config.CreateMapper();
        }
    }
}
