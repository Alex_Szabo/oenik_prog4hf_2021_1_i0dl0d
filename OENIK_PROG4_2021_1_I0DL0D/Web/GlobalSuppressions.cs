﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "False warning.", Scope = "member", Target = "~P:DrivingSchool.Web.Models.StudentListViewModel.StudentList")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "Set is also used, false warning.", Scope = "member", Target = "~P:DrivingSchool.Web.Models.StudentListViewModel.StudentList")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "I want to use this specific format.", Scope = "member", Target = "~M:DrivingSchool.Web.Controllers.StudentController.Edit(DrivingSchool.Web.Models.Student,System.String)~Microsoft.AspNetCore.Mvc.IActionResult")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "False warning.", Scope = "member", Target = "~M:DrivingSchool.Web.Controllers.StudentController.#ctor(DrivingSchool.Logic.IStudentsDrivingLogic,AutoMapper.IMapper)")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "I want to use this specific format.", Scope = "member", Target = "~M:DrivingSchool.Web.Controllers.StudentApiController.AddOneStudent(DrivingSchool.Web.Models.Student)~DrivingSchool.Web.ApiResult")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "I want to use this specific format.", Scope = "member", Target = "~M:DrivingSchool.Web.Controllers.StudentApiController.ModOneStudent(DrivingSchool.Web.Models.Student)~DrivingSchool.Web.ApiResult")]
