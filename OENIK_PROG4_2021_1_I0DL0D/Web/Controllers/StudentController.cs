﻿// <copyright file="StudentController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrivingSchool.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using DrivingSchool.Logic;
    using DrivingSchool.Web.Models;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Controller for students.
    /// </summary>
    public class StudentController : Controller
    {
        private IStudentsDrivingLogic logic;
        private IMapper mapper;
        private StudentListViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="StudentController"/> class.
        /// </summary>
        /// <param name="logic">Student logic.</param>
        /// <param name="mapper">Automapper for conversion.</param>
        public StudentController(IStudentsDrivingLogic logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;

            this.vm = new StudentListViewModel();
            this.vm.EditedStudent = new Models.Student();

            var students = logic.GetAllStudents();
            this.vm.StudentList = mapper.Map<IList<Data.Model.Student>, List<Models.Student>>(students);
        }

        /// <summary>
        /// Index action.
        /// </summary>
        /// <returns>Index page.</returns>
        public IActionResult Index()
        {
            this.ViewData["editAction"] = "AddNew";
            return this.View("StudentIndex", this.vm);
        }

        /// <summary>
        /// Details action.
        /// </summary>
        /// <param name="id">Id of student.</param>
        /// <returns>Details of one student.</returns>
        public IActionResult Details(int id)
        {
            return this.View("StudentDetails", this.GetStudentModel(id));
        }

        /// <summary>
        /// Remove action.
        /// </summary>
        /// <param name="id">Id of student.</param>
        /// <returns>Index page.</returns>
        public IActionResult Remove(int id)
        {
            this.TempData["editResult"] = "Delete Failed!";
            try
            {
                this.logic.RemoveStudent(id);
                this.TempData["editResult"] = "Delete Completed!";
            }
            catch (ArgumentException ex)
            {
                this.TempData["editResult"] = "Delete Failed! " + ex.Message;
            }

            return this.RedirectToAction(nameof(this.Index));
        }

        /// <summary>
        /// Edit action.
        /// </summary>
        /// <param name="id">Id of student.</param>
        /// <returns>Index page.</returns>
        public IActionResult Edit(int id)
        {
            this.ViewData["editAction"] = "Edit";
            this.vm.EditedStudent = this.GetStudentModel(id);
            return this.View("StudentIndex", this.vm);
        }

        /// <summary>
        /// Edit action.
        /// </summary>
        /// <param name="student">One student.</param>
        /// <param name="editAction">Action type.</param>
        /// <returns>Index page.</returns>
        [HttpPost]
        public IActionResult Edit(Models.Student student, string editAction)
        {
            if (this.ModelState.IsValid && student != null)
            {
                this.TempData["editResult"] = "Edit Ok!";
                if (editAction == "AddNew")
                {
                    try
                    {
                        var localFormat = "M/d/yyyy";
                        string sh = student.DateOfBirth.ToString(localFormat);
                        this.logic.AddStudent(student.FirstName, student.LastName, sh, student.Email, student.PhoneNumber, student.Sex);
                        this.TempData["editResult"] = "Add Completed!";
                    }
                    catch (ArgumentException ex)
                    {
                        this.TempData["editResult"] = "Add Failed!" + ex.Message;
                    }
                }
                else
                {
                    try
                    {
                        this.logic.ChangeEmail(student.StudentId, student.Email);
                        this.logic.ChangeFirstName(student.StudentId, student.FirstName);
                        this.logic.ChangeLastName(student.StudentId, student.LastName);
                        this.logic.ChangePhoneNumber(student.StudentId, student.PhoneNumber);
                        this.logic.ChangeSex(student.StudentId, student.Sex);
                        this.logic.ChangeDOB(student.StudentId, student.DateOfBirth.ToString("M/d/yyyy"));
                        this.TempData["editResult"] = "Edit Completed!";
                    }
                    catch (ArgumentException ex)
                    {
                        this.TempData["editResult"] = "Edit Failed! " + ex.Message;
                    }
                }

                return this.RedirectToAction(nameof(this.Index));
            }
            else
            {
                this.ViewData["editAction"] = "Edit";
                this.vm.EditedStudent = student;
                return this.View("StudentIndex", this.vm);
            }
        }

        private Models.Student GetStudentModel(int id)
        {
            Data.Model.Student oneStudent = this.logic.GetStudentId(id);
            return this.mapper.Map<Data.Model.Student, Models.Student>(oneStudent);
        }
    }
}
