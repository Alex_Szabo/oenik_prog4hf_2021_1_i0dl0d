﻿// <copyright file="StudentApiController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrivingSchool.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using DrivingSchool.Logic;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Api Controller for students.
    /// </summary>
    public class StudentApiController : Controller
    {
        private IStudentsDrivingLogic logic;
        private IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="StudentApiController"/> class.
        /// </summary>
        /// <param name="logic">IStudentsDrivingLogic.</param>
        /// <param name="mapper">IMapper.</param>
        public StudentApiController(IStudentsDrivingLogic logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;
        }

        /// <summary>
        /// Gets all students from database.
        /// </summary>
        /// <returns>List of all students.</returns>
        [HttpGet]
        [ActionName("all")]
        public IEnumerable<Models.Student> GetAll()
        {
            var students = this.logic.GetAllStudents();
            return this.mapper.Map<IList<Data.Model.Student>, List<Models.Student>>(students);
        }

        /// <summary>
        /// Removes one students from database.
        /// </summary>
        /// <param name="id">Id of student.</param>
        /// <returns>ApiResult indicating if it was successful.</returns>
        [HttpGet]
        [ActionName("del")]
        public ApiResult DelOneStudent(int id)
        {
            bool success = true;
            try
            {
                this.logic.RemoveStudent(id);
            }
            catch (ArgumentException)
            {
                success = false;
            }

            return new ApiResult() { OperationResult = success };
        }

        /// <summary>
        /// Adds one student to database.
        /// </summary>
        /// <param name="student">A new student.</param>
        /// <returns>ApiResult indicating if it was successful.</returns>
        [HttpPost]
        [ActionName("add")]
        public ApiResult AddOneStudent(Models.Student student)
        {
            bool success = true;
            try
            {
                var localFormat = "M/d/yyyy";
                string sh = student?.DateOfBirth.ToString(localFormat);
                string tday = DateTime.Now.ToString(localFormat);
                this.logic.AddStudent(student.FirstName, student.LastName, sh, student.Email, student.PhoneNumber, student.Sex);
            }
            catch (ArgumentException)
            {
                success = false;
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException)
            {
                success = false;
            }

            return new ApiResult() { OperationResult = success };
        }

        /// <summary>
        /// Modifies one student.
        /// </summary>
        /// <param name="student">Selected student.</param>
        /// <returns>ApiResult indicating if it was successful.</returns>
        [HttpPost]
        [ActionName("mod")]
        public ApiResult ModOneStudent(Models.Student student)
        {
            bool succress = true;
            try
            {
                if (student != null)
                {
                    this.logic.ChangeEmail(student.StudentId, student.Email);
                    this.logic.ChangeFirstName(student.StudentId, student.FirstName);
                    this.logic.ChangeLastName(student.StudentId, student.LastName);
                    this.logic.ChangePhoneNumber(student.StudentId, student.PhoneNumber);
                    this.logic.ChangeSex(student.StudentId, student.Sex);
                    this.logic.ChangeDOB(student.StudentId, student.DateOfBirth.ToString("M/d/yyyy"));
                }
            }
            catch (ArgumentException)
            {
                succress = false;
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException)
            {
                succress = false;
            }

            return new ApiResult() { OperationResult = succress };
        }
    }
}
