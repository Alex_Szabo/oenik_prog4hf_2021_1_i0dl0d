﻿// <copyright file="IStudentsDrivingLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrivingSchool.Logic
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;
    using DrivingSchool.Data.Model;

    /// <summary>
    /// Student and Lesson CRUD methods.
    /// </summary>
    public interface IStudentsDrivingLogic
    {
        /// <summary>
        /// Gets a Student element by Id.
        /// </summary>
        /// <param name="id">Id of Student.</param>
        /// <returns>Student with the Id.</returns>
        Student GetStudentId(int id);

        /// <summary>
        /// Gets a Lesson from Id.
        /// </summary>
        /// <param name="id">Id of Lesson.</param>
        /// <returns>Lesson with the Id.</returns>
        Lesson GetLessonId(int id);

        /// <summary>
        /// Adds a new Student.
        /// </summary>
        /// <param name="firstName">First Name of Student.</param>
        /// <param name="lastName">Last Name of Student.</param>
        /// <param name="dOB">Date of Birth of Student.</param>
        /// <param name="email">Email of Student.</param>
        /// <param name="phone">Phone number of Student.</param>
        /// <param name="sex">Sex of Student.</param>
        void AddStudent(string firstName, string lastName, string dOB, string email, string phone, string sex);

        /// <summary>
        /// Adds new Lesson.
        /// </summary>
        /// <param name="lessonDate">Date of Lesson.</param>
        /// <param name="lessonTime">Time length of Lesson.</param>
        /// <param name="distance">Distance of Lesson.</param>
        /// <param name="instructorId">Instructor's Id.</param>
        /// <param name="studentId">Student's Id.</param>
        /// <param name="carId">Car's Id.</param>
        void AddLesson(string lessonDate, int lessonTime, int distance, int instructorId, int studentId, int carId);

        /// <summary>
        /// Removes a Student by Id.
        /// </summary>
        /// <param name="id">Id of Student.</param>
        void RemoveStudent(int id);

        /// <summary>
        /// Removes a Lesson by Id.
        /// </summary>
        /// <param name="id">Id of Lesson.</param>
        void RemoveLesson(int id);

        /// <summary>
        /// Gets all Student elements.
        /// </summary>
        /// <returns>List of all Student elements.</returns>
        IList<Student> GetAllStudents();

        /// <summary>
        /// Gets all Lesson elements.
        /// </summary>
        /// <returns>List of all Lesson elements.</returns>
        IList<Lesson> GetAllLessons();

        /// <summary>
        /// Changes Email of Student.
        /// </summary>
        /// <param name="id">Id of Student.</param>
        /// <param name="newEmail">New Email for Student.</param>
        void ChangeEmail(int id, string newEmail);

        /// <summary>
        /// Changes FirstName of Student.
        /// </summary>
        /// <param name="id">Id of Student.</param>
        /// <param name="newFirstName">New FirstName for Student.</param>
        void ChangeFirstName(int id, string newFirstName);

        /// <summary>
        /// Changes LastName of Student.
        /// </summary>
        /// <param name="id">Id of Student.</param>
        /// <param name="newLastName">New LastName for Student.</param>
        void ChangeLastName(int id, string newLastName);

        /// <summary>
        /// Changes PhoneNumber of Student.
        /// </summary>
        /// <param name="id">Id of Student.</param>
        /// <param name="newPhoneNumber">New PhoneNumber for Student.</param>
        void ChangePhoneNumber(int id, string newPhoneNumber);

        /// <summary>
        /// Changes Sex of Student.
        /// </summary>
        /// <param name="id">Id of Student.</param>
        /// <param name="newSex">New Sex for Student.</param>
        void ChangeSex(int id, string newSex);

        /// <summary>
        /// Changes DOB of Student.
        /// </summary>
        /// <param name="id">Id of Student.</param>
        /// <param name="newDOB">New DOB for Student.</param>
        void ChangeDOB(int id, string newDOB);

        /// <summary>
        /// Changes Distance of Lesson.
        /// </summary>
        /// <param name="id">Id of Lesson.</param>
        /// <param name="newDistance">New Distance for Lesson.</param>
        void ChangeDistance(int id, int newDistance);

        /// <summary>
        /// Gets the average speed by sex.
        /// </summary>
        /// <returns>List with average speeds by sex.</returns>
        IList<AverageSpeedBySex> AverageSpeedBySex();

        /// <summary>
        /// Gets the average speed by sex.
        /// </summary>
        /// <returns>A task list of average speed by sex.</returns>
        Task<IList<AverageSpeedBySex>> AverageSpeedBySexTask();
    }
}
