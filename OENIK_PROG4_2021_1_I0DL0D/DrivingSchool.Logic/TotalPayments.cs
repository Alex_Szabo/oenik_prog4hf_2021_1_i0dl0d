﻿// <copyright file="TotalPayments.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrivingSchool.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Stores TotalPayment and InstructorId.
    /// </summary>
    public class TotalPayments
    {
        /// <summary>
        /// Gets or sets TotalPayment for an Instructor.
        /// </summary>
        public int TotalPayment { get; set; }

        /// <summary>
        /// Gets or sets InstructorId for an Instructor.
        /// </summary>
        public int InstructorId { get; set; }

        /// <summary>
        /// Checks if the other object is the same type and their equality.
        /// </summary>
        /// <param name="obj">The other object.</param>
        /// <returns>True, if they are the same type and equal.</returns>
        public override bool Equals(object obj)
        {
            if (obj is TotalPayments)
            {
                TotalPayments other = obj as TotalPayments;
                return this.InstructorId == other.InstructorId && this.TotalPayment == other.TotalPayment;
            }

            return false;
        }

        /// <summary>
        /// Generates object specific HashCode.
        /// </summary>
        /// <returns>Object specific integer.</returns>
        public override int GetHashCode()
        {
            return this.TotalPayment * this.InstructorId;
        }
    }
}
