﻿// <copyright file="AverageSpeedBySex.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrivingSchool.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Stores sex and average speed.
    /// </summary>
    public class AverageSpeedBySex
    {
        /// <summary>
        /// Gets or sets the average speed for a student.
        /// </summary>
        public double AverageSpeed { get; set; }

        /// <summary>
        /// Gets or sets the sex for a student.
        /// </summary>
        public string Sex { get; set; }

        /// <summary>
        /// Checks if the other object is the same type of this and their equality.
        /// </summary>
        /// <param name="obj">The other object.</param>
        /// <returns>True, if they are the same type and equal.</returns>
        public override bool Equals(object obj)
        {
            if (obj is AverageSpeedBySex)
            {
                AverageSpeedBySex other = obj as AverageSpeedBySex;
                return this.AverageSpeed == other.AverageSpeed && this.Sex == other.Sex;
            }

            return false;
        }

        /// <summary>
        /// Generates an object specific HashCode.
        /// </summary>
        /// <returns>Object specific integer.</returns>
        public override int GetHashCode()
        {
            return this.Sex.Length * (int)this.AverageSpeed;
        }
    }
}
