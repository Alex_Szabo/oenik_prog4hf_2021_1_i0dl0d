﻿// <copyright file="VehicleUsageLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrivingSchool.Logic
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Driving.School.Repository;
    using DrivingSchool.Data.Model;
    using Microsoft.Extensions.Caching.Memory;

    /// <summary>
    /// Manages Cars.
    /// </summary>
    public class VehicleUsageLogic : IVehicleUsageLogic
    {
        /// <summary>
        /// Gets the Car Repository for Database access.
        /// </summary>
        private ICarRepository carRepo;

        /// <summary>
        /// Gets the Lesson Repository for Database access.
        /// </summary>
        private ILessonRepository lessonRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="VehicleUsageLogic"/> class.
        /// </summary>
        /// <param name="carRepo">Car's Repository interface.</param>
        /// <param name="lessonRepo">Lesson's Repository interface.</param>
        public VehicleUsageLogic(ICarRepository carRepo, ILessonRepository lessonRepo)
        {
            this.carRepo = carRepo;
            this.lessonRepo = lessonRepo;
        }

        /// <summary>
        /// Changes the LicensePlate of a car.
        /// </summary>
        /// <param name="id">Id of Car.</param>
        /// <param name="newLicensePlate">New LicensePlate for a car.</param>
        public void ChangeLicensePlate(int id, string newLicensePlate)
        {
            this.carRepo.ChangeLicensePlate(id, newLicensePlate);
        }

        /// <summary>
        /// Gets all Car elements.
        /// </summary>
        /// <returns>List of all Car elements.</returns>
        public IList<Car> GetAllCars()
        {
            return this.carRepo.GetAll().ToList();
        }

        /// <summary>
        /// Gets a Car by Id.
        /// </summary>
        /// <param name="id">Id of Car.</param>
        /// <returns>Car with the Id.</returns>
        public Car GetCarId(int id)
        {
            return this.carRepo.GetOne(id);
        }

        /// <summary>
        /// Adds a new Car.
        /// </summary>
        /// <param name="brand">Brand of Car.</param>
        /// <param name="type">Type of Car.</param>
        /// <param name="manufacturingYear">ManufacturingYear of Car.</param>
        /// <param name="licensePlate">LicensePlate of Car.</param>
        /// <param name="transmission">Transmission type of Car. True if automatic.</param>
        public void AddCar(string brand, string type, int manufacturingYear, string licensePlate, bool transmission)
        {
            Car newCar = new Car() { Brand = brand, Type = type, ManufacturingYear = manufacturingYear, LicensePlate = licensePlate, Automatic = transmission };
            this.carRepo.Add(newCar);
        }

        /// <summary>
        /// Removes a Car by Id.
        /// </summary>
        /// <param name="id">Id of Car.</param>
        public void RemoveCar(int id)
        {
            this.carRepo.Remove(id);
        }

        /// <summary>
        /// Gets total distances by cars.
        /// </summary>
        /// <returns>List of cars by total distances.</returns>
        public IList<TotalDistances> TotalDistances()
        {
            var q = from x in this.carRepo.GetAll()
                    join y in this.lessonRepo.GetAll() on x.CarId equals y.CarId
                    let item = new { Car = x.CarId, y.Distance }
                    group item by x.CarId into g
                    select new TotalDistances()
                    {
                        CarId = g.Key,
                        TotalDistance = g.Sum(item => item.Distance),
                    };

            return q.ToList();
        }

        /// <summary>
        /// Gets the total distance for each car.
        /// </summary>
        /// <returns>A task list of total distance for each car.</returns>
        public Task<IList<TotalDistances>> TotalDistancesTask()
        {
            return Task.Run(() => this.TotalDistances());
        }
    }
}
