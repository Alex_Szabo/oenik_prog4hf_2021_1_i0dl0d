﻿// <copyright file="IVehicleUsageLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrivingSchool.Logic
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;
    using DrivingSchool.Data.Model;

    /// <summary>
    /// CRUD methods for Car.
    /// </summary>
    public interface IVehicleUsageLogic
    {
        /// <summary>
        /// Gets a Car by Id.
        /// </summary>
        /// <param name="id">Id of Car.</param>
        /// <returns>Car with the Id.</returns>
        Car GetCarId(int id);

        /// <summary>
        /// Removes a Car by Id.
        /// </summary>
        /// <param name="id">Id of the Car.</param>
        void RemoveCar(int id);

        /// <summary>
        /// Changes LicensePlate of a Car.
        /// </summary>
        /// <param name="id">Id of Car.</param>
        /// <param name="newLicensePlate">New LicensePlate for a Car.</param>
        void ChangeLicensePlate(int id, string newLicensePlate);

        /// <summary>
        /// Adds a new Car element.
        /// </summary>
        /// <param name="brand">Brand of Car.</param>
        /// <param name="type">Type of Car.</param>
        /// <param name="manufacturingYear">ManufacturingYear of Car.</param>
        /// <param name="licensePlate">LicensePlate of Car.</param>
        /// <param name="transmission">Transmission type of Car. True if automatic.</param>
        void AddCar(string brand, string type, int manufacturingYear, string licensePlate, bool transmission);

        /// <summary>
        /// Gets all Car elements.
        /// </summary>
        /// <returns>List of all Car elements.</returns>
        IList<Car> GetAllCars();

        /// <summary>
        /// Calculates total distances for each car.
        /// </summary>
        /// <returns>List with total distances by car.</returns>
        IList<TotalDistances> TotalDistances();

        /// <summary>
        /// Gets the total distances for each car.
        /// </summary>
        /// <returns>A task list of total distances for each car.</returns>
        Task<IList<TotalDistances>> TotalDistancesTask();
    }
}
