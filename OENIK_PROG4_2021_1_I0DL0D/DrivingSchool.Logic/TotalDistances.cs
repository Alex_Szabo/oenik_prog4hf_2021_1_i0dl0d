﻿// <copyright file="TotalDistances.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrivingSchool.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Stores the TotalDistance and CarId.
    /// </summary>
    public class TotalDistances
    {
        /// <summary>
        /// Gets or sets the TotalDistance for a car.
        /// </summary>
        public int TotalDistance { get; set; }

        /// <summary>
        /// Gets or sets the CarId for a car.
        /// </summary>
        public int CarId { get; set; }

        /// <summary>
        /// Checks if the other object is the same type and their equality.
        /// </summary>
        /// <param name="obj">The other object.</param>
        /// <returns>True, if they are the same type and equal.</returns>
        public override bool Equals(object obj)
        {
            if (obj is TotalDistances)
            {
                TotalDistances other = obj as TotalDistances;
                return this.CarId == other.CarId && this.TotalDistance == other.TotalDistance;
            }

            return false;
        }

        /// <summary>
        /// Generates object specific HashCode.
        /// </summary>
        /// <returns>Object specific integer.</returns>
        public override int GetHashCode()
        {
            return this.TotalDistance * this.CarId;
        }
    }
}
