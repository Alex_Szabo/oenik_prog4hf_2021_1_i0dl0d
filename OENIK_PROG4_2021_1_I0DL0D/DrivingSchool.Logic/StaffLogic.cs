﻿// <copyright file="StaffLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrivingSchool.Logic
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Driving.School.Repository;
    using DrivingSchool.Data.Model;

    /// <summary>
    /// Manages CRUD and PriceChange for Instructors.
    /// </summary>
    public class StaffLogic : IStaffLogic
    {
        /// <summary>
        /// Instructor interface to access the Database through it.
        /// </summary>
        private IInstructorRepository instructorRepo;

        /// <summary>
        /// Lesson interface to access the Database through it.
        /// </summary>
        private ILessonRepository lessonRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="StaffLogic"/> class.
        /// </summary>
        /// <param name="instructorRepository">Instructors Repository interface.</param>
        /// <param name="lessonRepository">Lessons Repository interface.</param>
        public StaffLogic(IInstructorRepository instructorRepository, ILessonRepository lessonRepository)
        {
            this.instructorRepo = instructorRepository;
            this.lessonRepository = lessonRepository;
        }

        /// <summary>
        /// Changes the Price of an Instructor.
        /// </summary>
        /// <param name="id">Id of Instructor.</param>
        /// <param name="price">New Price for an Instructor.</param>
        public void ChangeInstructorPrice(int id, int price)
        {
            this.instructorRepo.ChangePrice(id, price);
        }

        /// <summary>
        /// Gets all Instructor elements.
        /// </summary>
        /// <returns>List of all Instructor elements.</returns>
        public IList<Instructor> GetAllInstructors()
        {
            return this.instructorRepo.GetAll().ToList();
        }

        /// <summary>
        /// Gets an Instructor by Id.
        /// </summary>
        /// <param name="id">Id of Instructor.</param>
        /// <returns>Instructor with the Id.</returns>
        public Instructor GetInstructorId(int id)
        {
            return this.instructorRepo.GetOne(id);
        }

        /// <summary>
        /// Adds a new Instructor.
        /// </summary>
        /// <param name="firstName">First Name of Instructor.</param>
        /// <param name="lastName">Last Name of Instructor.</param>
        /// <param name="dOB">Date of Birth of Instructor.</param>
        /// <param name="dOJ">Date of Join of Instructor.</param>
        /// <param name="price">Price of Instructor.</param>
        /// <param name="sex">Sex of Instructor.</param>
        /// <param name="phonenumber">Phone number of Instructor.</param>
        public void AddInstructor(string firstName, string lastName, string dOB, string dOJ, int price, string sex, string phonenumber)
        {
            Instructor newInstructor = new Instructor() { FirstName = firstName, LastName = lastName, DateOfBirth = DateTime.ParseExact(dOB, "yyyy.MM.dd.", null), DateOfJoin = DateTime.ParseExact(dOJ, "yyyy.MM.dd.", null), Price = price, Sex = sex, PhoneNumber = phonenumber };
            this.instructorRepo.Add(newInstructor);
        }

        /// <summary>
        /// Remove an Instructor by Id.
        /// </summary>
        /// <param name="id">Id of Instructor.</param>
        public void RemoveInstructor(int id)
        {
            this.instructorRepo.Remove(id);
        }

        /// <summary>
        /// Gets the total payment for instructors.
        /// </summary>
        /// <returns>List of total payments by instructors.</returns>
        public IList<TotalPayments> TotalPayments()
        {
            var q = from x in this.instructorRepo.GetAll()
                    join y in this.lessonRepository.GetAll() on x.InstructorId equals y.InstructorId
                    group x by x.InstructorId into g
                    select new TotalPayments
                    {
                        InstructorId = g.Key,
                        TotalPayment = g.Sum(a => a.Price),
                    };
            return q.ToList();
        }

        /// <summary>
        /// Gets the total payments for instructors.
        /// </summary>
        /// <returns>A task list of total payments for instructors.</returns>
        public Task<IList<TotalPayments>> TotalPaymentsTask()
        {
            return Task.Run(() => this.TotalPayments());
        }
    }
}
