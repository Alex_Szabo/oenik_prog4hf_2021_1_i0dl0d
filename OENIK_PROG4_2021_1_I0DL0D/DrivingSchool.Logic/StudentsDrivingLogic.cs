﻿// <copyright file="StudentsDrivingLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrivingSchool.Logic
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Driving.School.Repository;
    using DrivingSchool.Data.Model;

    /// <summary>
    /// Manages students and lessons.
    /// </summary>
    public class StudentsDrivingLogic : IStudentsDrivingLogic
    {
        /// <summary>
        /// Gets StudentRepository for Database access.
        /// </summary>
        private IStudentRepository studentrepo;

        /// <summary>
        /// Gets LessonRepository for Database access.
        /// </summary>
        private ILessonRepository lessonrepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="StudentsDrivingLogic"/> class.
        /// </summary>
        /// <param name="studentrepo">Student's repository.</param>
        /// <param name="lessonrepo">Lesson's repository.</param>
        public StudentsDrivingLogic(IStudentRepository studentrepo, ILessonRepository lessonrepo)
        {
            this.studentrepo = studentrepo;
            this.lessonrepo = lessonrepo;
        }

        /// <summary>
        /// Gets all Student elements.
        /// </summary>
        /// <returns>List of all Student elements.</returns>
        public IList<Student> GetAllStudents()
        {
            return this.studentrepo.GetAll().ToList();
        }

        /// <summary>
        /// Gets all Lesson elements.
        /// </summary>
        /// <returns>List of all Lesson elements.</returns>
        public IList<Lesson> GetAllLessons()
        {
            return this.lessonrepo.GetAll().ToList();
        }

        /// <summary>
        /// Gets a Student by Id.
        /// </summary>
        /// <param name="id">Id of Student.</param>
        /// <returns>Student with the Id.</returns>
        public Student GetStudentId(int id)
        {
            return this.studentrepo.GetOne(id);
        }

        /// <summary>
        /// Gets a Lesson by id.
        /// </summary>
        /// <param name="id">Id of Lesson.</param>
        /// <returns>Lesson with the Id.</returns>
        public Lesson GetLessonId(int id)
        {
            return this.lessonrepo.GetOne(id);
        }

        /// <summary>
        /// Adds a new Student.
        /// </summary>
        /// <param name="firstName">First Name of Student.</param>
        /// <param name="lastName">Last Name of Student.</param>
        /// <param name="dOB">Date of Birth of Student.</param>
        /// <param name="email">Email of Student.</param>
        /// <param name="phone">Sex of Student.</param>
        /// <param name="sex">Phone number of Student.</param>
        public void AddStudent(string firstName, string lastName, string dOB, string email, string phone, string sex)
        {
            Student newStudent = new Student() { FirstName = firstName, LastName = lastName, DateOfBirth = DateTime.ParseExact(dOB, "M/d/yyyy", null), Email = email, Sex = sex, PhoneNumber = phone };
            this.studentrepo.Add(newStudent);
        }

        /// <summary>
        /// Adds a new Lesson.
        /// </summary>
        /// <param name="lessonDate">Date of Lesson.</param>
        /// <param name="lessonTime">Time length of Lesson.</param>
        /// <param name="distance">Distance of Lesson.</param>
        /// <param name="instructorId">Instructor's Id.</param>
        /// <param name="studentId">Student's Id.</param>
        /// <param name="carId">Car's Id.</param>
        public void AddLesson(string lessonDate, int lessonTime, int distance, int instructorId, int studentId, int carId)
        {
            Lesson newLesson = new Lesson() { LessonDate = DateTime.ParseExact(lessonDate, "yyyy.MM.dd.", null), LessonTime = lessonTime, Distance = distance, InstructorId = instructorId, StudentId = studentId, CarId = carId };
            this.lessonrepo.Add(newLesson);
        }

        /// <summary>
        /// Changes Email of Student.
        /// </summary>
        /// <param name="id">Id of Student.</param>
        /// <param name="newEmail">New Email for Student.</param>
        public void ChangeEmail(int id, string newEmail)
        {
            this.studentrepo.ChangeEmail(id, newEmail);
        }

        /// <summary>
        /// Changes FirstName of Student.
        /// </summary>
        /// <param name="id">Id of Student.</param>
        /// <param name="newFirstName">New FirstName for Student.</param>
        public void ChangeFirstName(int id, string newFirstName)
        {
            this.studentrepo.ChangeFirstName(id, newFirstName);
        }

        /// <summary>
        /// Changes LastName of Student.
        /// </summary>
        /// <param name="id">Id of Student.</param>
        /// <param name="newLastName">New LastName for Student.</param>
        public void ChangeLastName(int id, string newLastName)
        {
            this.studentrepo.ChangeLastName(id, newLastName);
        }

        /// <summary>
        /// Changes PhoneNUmber of Student.
        /// </summary>
        /// <param name="id">Id of Student.</param>
        /// <param name="newPhoneNumber">New PhoneNUmber for Student.</param>
        public void ChangePhoneNumber(int id, string newPhoneNumber)
        {
            this.studentrepo.ChangePhoneNumber(id, newPhoneNumber);
        }

        /// <summary>
        /// Changes Sex of Student.
        /// </summary>
        /// <param name="id">Id of Student.</param>
        /// <param name="newSex">New Sex for Student.</param>
        public void ChangeSex(int id, string newSex)
        {
            this.studentrepo.ChangeSex(id, newSex);
        }

        /// <summary>
        /// Changes DOB of Student.
        /// </summary>
        /// <param name="id">Id of Student.</param>
        /// <param name="newDOB">New DOB for Student.</param>
        public void ChangeDOB(int id, string newDOB)
        {
            DateTime newdate = DateTime.ParseExact(newDOB, "M/d/yyyy", null);
            this.studentrepo.ChangeDOB(id, newdate);
        }

        /// <summary>
        /// Changes Distance of Lesson.
        /// </summary>
        /// <param name="id">Id of Lesson.</param>
        /// <param name="newDistance">New Distance for Lesson.</param>
        public void ChangeDistance(int id, int newDistance)
        {
            this.lessonrepo.ChangeDistance(id, newDistance);
        }

        /// <summary>
        /// Removes a Student by Id.
        /// </summary>
        /// <param name="id">Id of Student.</param>
        public void RemoveStudent(int id)
        {
            this.studentrepo.Remove(id);
        }

        /// <summary>
        /// Removes a Lesson by Id.
        /// </summary>
        /// <param name="id">Id of Lesson.</param>
        public void RemoveLesson(int id)
        {
            this.lessonrepo.Remove(id);
        }

        /// <summary>
        /// Gets the average speed by sex.
        /// </summary>
        /// <returns>List with average speeds by sex.</returns>
        public IList<AverageSpeedBySex> AverageSpeedBySex()
        {
            var q = from x in this.lessonrepo.GetAll()
                    join y in this.studentrepo.GetAll() on x.StudentId equals y.StudentId
                    group x by y.Sex into g
                    select new AverageSpeedBySex
                    {
                        Sex = g.Key,
                        AverageSpeed = g.Average(b => (60 * b.Distance / b.LessonTime)),
                    };
            return q.ToList();
        }

        /// <summary>
        /// Gets the average speed by sex.
        /// </summary>
        /// <returns>A task list of average speed by sex.</returns>
        public Task<IList<AverageSpeedBySex>> AverageSpeedBySexTask()
        {
            return Task.Run(() => this.AverageSpeedBySex());
        }
    }
}
