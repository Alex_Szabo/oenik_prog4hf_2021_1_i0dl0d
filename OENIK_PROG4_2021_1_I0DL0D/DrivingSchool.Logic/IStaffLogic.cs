﻿// <copyright file="IStaffLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrivingSchool.Logic
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using DrivingSchool.Data.Model;

    /// <summary>
    /// Interface that specify CRUD methods for StaffLogic.
    /// </summary>
    public interface IStaffLogic
    {
        /// <summary>
        /// Gets an Instructor from an Id.
        /// </summary>
        /// <param name="id">Id of Instructor.</param>
        /// <returns>Instructor with the Id.</returns>
        Instructor GetInstructorId(int id);

        /// <summary>
        /// Changes the Price for an Instructor.
        /// </summary>
        /// <param name="id">Id of Instructor.</param>
        /// <param name="price">New Price for Instructor.</param>
        void ChangeInstructorPrice(int id, int price);

        /// <summary>
        /// Removes an Instructor by Id.
        /// </summary>
        /// <param name="id">Id of Instructor.</param>
        void RemoveInstructor(int id);

        /// <summary>
        /// Adds a new Instructor element.
        /// </summary>
        /// <param name="firstName">First Name of Instructor.</param>
        /// <param name="lastName">Last Name of Instructor.</param>
        /// <param name="dOB">Date of Birth of Instructor.</param>
        /// <param name="dOJ">Date of Join of Instructor.</param>
        /// <param name="price">Price of Instructor.</param>
        /// <param name="sex">Sex of Instructor.</param>
        /// <param name="phonenumber">Phone number of Instructor.</param>
        void AddInstructor(string firstName, string lastName, string dOB, string dOJ, int price, string sex, string phonenumber);

        /// <summary>
        /// Gets all Instructor elements.
        /// </summary>
        /// <returns>List of all Instructors.</returns>
        IList<Instructor> GetAllInstructors();

        /// <summary>
        /// Gets the total payment for instructors.
        /// </summary>
        /// <returns>List of total payments by instructors.</returns>
        IList<TotalPayments> TotalPayments();

        /// <summary>
        /// Gets the total payment for instructors.
        /// </summary>
        /// <returns>A task list of total payments by instructors.</returns>
        Task<IList<TotalPayments>> TotalPaymentsTask();
    }
}
