﻿// <copyright file="MaleToRadioButtonConverter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrivingSchool.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Controls;
    using System.Windows.Data;

    /// <summary>
    /// Converts between female student and radiobutton.
    /// </summary>
    public class MaleToRadioButtonConverter : IValueConverter
    {
        /// <summary>
        /// Checks a students sex and notifies the radiobutton.
        /// </summary>
        /// <param name="value">Student's sex.</param>
        /// <param name="targetType">Type of the binding target.</param>
        /// <param name="parameter">Converter parameter.</param>
        /// <param name="culture">Culture for converter.</param>
        /// <returns>True if male, false if female.</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((string)value == "male")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Checks a students sex and notifies the radiobutton.
        /// </summary>
        /// <param name="value">Student's sex.</param>
        /// <param name="targetType">Type of the binding target.</param>
        /// <param name="parameter">Converter parameter.</param>
        /// <param name="culture">Culture for converter.</param>
        /// <returns>True if male, false if female.</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool buttonofmale = (bool)value;
            if (buttonofmale == true)
            {
                return "male";
            }
            else
            {
                return "female";
            }
        }
    }
}
