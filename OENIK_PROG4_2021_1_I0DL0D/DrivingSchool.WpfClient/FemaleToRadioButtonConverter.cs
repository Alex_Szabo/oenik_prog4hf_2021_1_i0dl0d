﻿// <copyright file="FemaleToRadioButtonConverter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrivingSchool.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Controls;
    using System.Windows.Data;

    /// <summary>
    /// Converts between female student and radiobutton.
    /// </summary>
    public class FemaleToRadioButtonConverter : IValueConverter
    {
        /// <summary>
        /// Checks a students sex and notifies the radiobutton.
        /// </summary>
        /// <param name="value">Student's sex.</param>
        /// <param name="targetType">Type of the binding target.</param>
        /// <param name="parameter">Converter parameter.</param>
        /// <param name="culture">Culture for converter.</param>
        /// <returns>True if female, false if male.</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((string)value == "female")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Gets student's sex from radiobutton.
        /// </summary>
        /// <param name="value">Radiobutton ischecked property.</param>
        /// <param name="targetType">Convert to the type.</param>
        /// <param name="parameter">Converter parameter.</param>
        /// <param name="culture">Culture for converter.</param>
        /// <returns>Female if true, male if false.</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool buttonofmale = (bool)value;
            if (buttonofmale == true)
            {
                return "female";
            }
            else
            {
                return "male";
            }
        }
    }
}
