﻿// <copyright file="MainLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrivingSchool.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Text.Json;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// MainLogic for student operations.
    /// </summary>
    public class MainLogic
    {
        private string url = "http://localhost:1846/StudentApi/";
        private HttpClient client = new HttpClient();
        private JsonSerializerOptions jsonSerializerOptions = new JsonSerializerOptions(JsonSerializerDefaults.Web);

        /// <summary>
        /// Calls api to get all students.
        /// </summary>
        /// <returns>List of students.</returns>
        public ICollection<StudentVM> ApiGetStudents()
        {
            string json = this.client.GetStringAsync(new Uri(this.url + "all")).Result;
            var list = JsonSerializer.Deserialize<List<StudentVM>>(json, this.jsonSerializerOptions);

            return list;
        }

        /// <summary>
        /// Calls  api to remove one student.
        /// </summary>
        /// <param name="student">Selected student.</param>
        public void ApiDelStudent(StudentVM student)
        {
            bool success = false;
            if (student != null)
            {
                string json = this.client.GetStringAsync(new Uri(this.url + "del/" + student.StudentId.ToString())).Result;
                JsonDocument doc = JsonDocument.Parse(json);
                success = doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
            }

            SendMessage(success);
        }

        /// <summary>
        /// Edit one student.
        /// </summary>
        /// <param name="student">A student.</param>
        /// <param name="editorFunc">EditorFunc.</param>
        public void EditStudent(StudentVM student, Func<StudentVM, bool> editorFunc)
        {
            StudentVM clone = new StudentVM();
            if (student != null)
            {
                clone.CopyFrom(student);
            }

            bool? success = editorFunc?.Invoke(clone);
            if (success == true)
            {
                if (student != null)
                {
                    success = this.ApiEditStudent(clone, true);
                }
                else
                {
                    success = this.ApiEditStudent(clone, false);
                }
            }

            SendMessage(success == true);
        }

        /// <summary>
        /// Sends message about operation success.
        /// </summary>
        /// <param name="success">Result of the operation.</param>
        private static void SendMessage(bool success)
        {
            string msg = success ? "Operation completed successfully" : " Operation failed";
            Messenger.Default.Send(msg, "StudentResult");
        }

        private bool ApiEditStudent(StudentVM student, bool isEditing)
        {
            if (student == null)
            {
                return false;
            }

            string myUrl = this.url + (isEditing ? "mod" : "add");

            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing)
            {
                postData.Add("StudentId", student.StudentId.ToString());
            }

            postData.Add("FirstName", student.FirstName);
            postData.Add("LastName", student.LastName);
            postData.Add("DateOfBirth", student.DateOfBirth);
            postData.Add("PhoneNumber", student.PhoneNumber);
            postData.Add("Email", student.Email);
            postData.Add("Sex", student.Sex);

            string json = this.client.PostAsync(new Uri(myUrl), new FormUrlEncodedContent(postData)).
                Result.Content.ReadAsStringAsync().Result;
            JsonDocument doc = JsonDocument.Parse(json);
            return doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
        }
    }
}
