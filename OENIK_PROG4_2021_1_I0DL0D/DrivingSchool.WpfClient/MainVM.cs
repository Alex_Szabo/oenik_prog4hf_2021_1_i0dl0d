﻿// <copyright file="MainVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrivingSchool.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// Main ViewModel.
    /// </summary>
    public class MainVM : ViewModelBase
    {
        private MainLogic logic;
        private StudentVM selectedStudent;
        private ObservableCollection<StudentVM> allStudents;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        /// <param name="logic">MainLogic.</param>
        public MainVM(MainLogic logic)
        {
            this.logic = logic;

            this.LoadCmd = new RelayCommand(() => this.AllStudents = new ObservableCollection<StudentVM>(this.logic.ApiGetStudents()));
            this.DelCmd = new RelayCommand(() => this.logic.ApiDelStudent(this.selectedStudent));
            this.AddCmd = new RelayCommand(() => this.logic.EditStudent(null, this.EditorFunc));
            this.ModCmd = new RelayCommand(() => this.logic.EditStudent(this.selectedStudent, this.EditorFunc));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        public MainVM()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<MainLogic>())
        {
        }

        /// <summary>
        /// Gets or sets the current student selected from list.
        /// </summary>
        public StudentVM SelectedStudent
        {
            get { return this.selectedStudent; }
            set { this.Set(ref this.selectedStudent, value); }
        }

        /// <summary>
        /// Gets or sets all students in the list.
        /// </summary>
        public ObservableCollection<StudentVM> AllStudents
        {
            get { return this.allStudents; }
            set { this.Set(ref this.allStudents, value); }
        }

        /// <summary>
        /// Gets or sets indicating edit was successful.
        /// </summary>
        public Func<StudentVM, bool> EditorFunc { get; set; }

        /// <summary>
        /// Gets add command.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets del command.
        /// </summary>
        public ICommand DelCmd { get; private set; }

        /// <summary>
        /// Gets mod command.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Gets load command.
        /// </summary>
        public ICommand LoadCmd { get; private set; }
    }
}
