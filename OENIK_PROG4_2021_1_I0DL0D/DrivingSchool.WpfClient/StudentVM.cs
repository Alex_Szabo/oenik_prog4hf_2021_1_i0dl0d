﻿// <copyright file="StudentVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrivingSchool.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// A student for the UI.
    /// </summary>
    public class StudentVM : ObservableObject
    {
        private string firstName;
        private string lastname;
        private string dateOfBirth;
        private string sex;
        private string phoneNumber;
        private string email;
        private int studentId;

        /// <summary>
        /// Gets or sets id of student.
        /// </summary>
        public int StudentId
        {
            get { return this.studentId; }
            set { this.Set(ref this.studentId, value); }
        }

        /// <summary>
        /// Gets or sets firstName of student.
        /// </summary>
        public string FirstName
        {
            get { return this.firstName; }
            set { this.Set(ref this.firstName, value); }
        }

        /// <summary>
        /// Gets or sets lastName of student.
        /// </summary>
        public string LastName
        {
            get { return this.lastname; }
            set { this.Set(ref this.lastname, value); }
        }

        /// <summary>
        /// Gets or sets dateOfBrith of student.
        /// </summary>
        public string DateOfBirth
        {
            get { return this.dateOfBirth; }
            set { this.Set(ref this.dateOfBirth, value); }
        }

        /// <summary>
        /// Gets or sets phoneNumber of student.
        /// </summary>
        public string PhoneNumber
        {
            get { return this.phoneNumber; }
            set { this.Set(ref this.phoneNumber, value); }
        }

        /// <summary>
        /// Gets or sets email of student.
        /// </summary>
        public string Email
        {
            get { return this.email; }
            set { this.Set(ref this.email, value); }
        }

        /// <summary>
        /// Gets or sets sex of student.
        /// </summary>
        public string Sex
        {
            get { return this.sex; }
            set { this.Set(ref this.sex, value); }
        }

        /// <summary>
        /// Creates a clone of a student.
        /// </summary>
        /// <param name="stud">The old student.</param>
        public void CopyFrom(StudentVM stud)
        {
            if (stud == null)
            {
                return;
            }

            this.GetType().GetProperties().ToList().ForEach(
                property => property.SetValue(this, property.GetValue(stud)));
        }
    }
}
