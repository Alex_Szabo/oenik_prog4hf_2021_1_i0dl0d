﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "I want to use this specific format.", Scope = "member", Target = "~M:DrivingSchool.WpfClient.MainLogic.ApiDelStudent(DrivingSchool.WpfClient.StudentVM)")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "I want to use this specific format.", Scope = "member", Target = "~M:DrivingSchool.WpfClient.MainLogic.ApiEditStudent(DrivingSchool.WpfClient.StudentVM,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("Reliability", "CA2000:Dispose objects before losing scope", Justification = "False warning.", Scope = "member", Target = "~M:DrivingSchool.WpfClient.MainLogic.ApiEditStudent(DrivingSchool.WpfClient.StudentVM,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("Design", "CA1001:Types that own disposable fields should be disposable", Justification = "False warning.", Scope = "type", Target = "~T:DrivingSchool.WpfClient.MainLogic")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "It need to be set too.", Scope = "member", Target = "~P:DrivingSchool.WpfClient.MainVM.AllStudents")]
