﻿// <copyright file="ToStringAttribute.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrivingSchool.Data.Model
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using Microsoft.EntityFrameworkCore.Metadata.Internal;

    /// <summary>
    /// Attribute that marks important elements of table for ToString method.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class ToStringAttribute : Attribute
    {
    }
}
