﻿// <copyright file="Instructor.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrivingSchool.Data.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Reflection;
    using System.Text;

    /// <summary>
    /// Table of instructors.
    /// </summary>
    [Table("Instructors")]
    public class Instructor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Instructor"/> class.
        /// Sets the lesson to be a HashSet.
        /// </summary>
        public Instructor()
        {
            this.Lesson = new HashSet<Lesson>();
        }

        /// <summary>
        /// Gets or sets instructor's id.
        /// </summary>
        [Key]
        [ToString]
        public int InstructorId { get; set; }

        /// <summary>
        /// Gets or sets first name of instructor.
        /// </summary>
        [Required]
        [ToString]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets last name of instructor.
        /// </summary>
        [Required]
        [ToString]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets date of birth of instructor.
        /// </summary>
        [ToString]
        public DateTime DateOfBirth { get; set; }

        /// <summary>
        /// Gets or sets instructor's sex.
        /// </summary>
        [ToString]
        public string Sex { get; set; }

        /// <summary>
        /// Gets or sets instructor's date of join.
        /// </summary>
        [Required]
        [ToString]
        public DateTime DateOfJoin { get; set; }

        /// <summary>
        /// Gets or sets instructor's date of left.
        /// </summary>
        public DateTime DateOfLeft { get; set; }

        /// <summary>
        /// Gets or sets instructor's phone number.
        /// </summary>
        [ToString]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Gets or sets instructor's price.
        /// </summary>
        [Required]
        [ToString]
        public int Price { get; set; }

        /// <summary>
        /// Gets the lesson of instructor.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Lesson> Lesson { get; }

        /// <summary>
        /// ToString method to write out all important elements with their values of this table.
        /// </summary>
        /// <returns>A string that has all elements with values of this table.</returns>
        public override string ToString()
        {
            string s = string.Empty;

            foreach (var item in this.GetType().GetProperties().Where(x => x.GetCustomAttribute<ToStringAttribute>() != null))
            {
                s += " ";
                s += item.Name + "\t=> ";
                s += item.GetValue(this);
                s += "\n";
            }

            s += " ";
            return s;
        }

        /// <summary>
        /// Checks if the other object is the same type and their equality.
        /// </summary>
        /// <param name="obj">The other object.</param>
        /// <returns>True, if they are the same type and equal..</returns>
        public override bool Equals(object obj)
        {
            if (obj is Instructor)
            {
                Instructor other = obj as Instructor;
                return this.FirstName == other.FirstName &&
                    this.LastName == other.LastName &&
                    this.Price == other.Price &&
                    this.DateOfBirth == other.DateOfBirth &&
                    this.DateOfJoin == other.DateOfJoin;
            }

            return false;
        }

        /// <summary>
        /// Generates object specific HashCode.
        /// </summary>
        /// <returns>Object specific integer.</returns>
        public override int GetHashCode()
        {
            return this.DateOfBirth.Year * this.DateOfBirth.Month * this.DateOfBirth.Day * this.Price;
        }
    }
}
