﻿// <copyright file="DrivingSchoolContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrivingSchool.Data.Model
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Configures the database and load it with data.
    /// </summary>
    public class DrivingSchoolContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DrivingSchoolContext"/> class.
        /// Ensures that the database is exists.
        /// </summary>
        public DrivingSchoolContext()
        {
            this.Database.EnsureCreated();
        }

        /// <summary>
        /// Gets or sets the instructors dataset.
        /// </summary>
        public virtual DbSet<Instructor> Instructors { get; set; }

        /// <summary>
        /// Gets or sets the student's dataset.
        /// </summary>
        public virtual DbSet<Student> Students { get; set; }

        /// <summary>
        /// Gets or sets the car's dataset.
        /// </summary>
        public virtual DbSet<Car> Cars { get; set; }

        /// <summary>
        /// Gets or sets the lesson's dataset.
        /// </summary>
        public virtual DbSet<Lesson> Lessons { get; set; }

        /// <summary>
        /// Checks if database is configured and configures it if not.
        /// </summary>
        /// <param name="optionsBuilder">
        /// Sets up UseLazyLoadingProxies and UseSQLServer.
        /// </param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!(optionsBuilder is null))
            {
                if (!optionsBuilder.IsConfigured)
                {
                    optionsBuilder
                        .UseLazyLoadingProxies()
                        .UseSqlServer(@"Data Source = (LocalDB)\MSSQLLocalDB; AttachDbFilename = |DataDirectory|\DrivingSchoolDatabase.mdf; Integrated Security = True");
                }
            }
        }

        /// <summary>
        /// Creates data for database.
        /// </summary>
        /// <param name="modelBuilder">
        /// A ModelBuilder instance that configures data in database.
        /// </param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            Instructor i0 = new Instructor() { InstructorId = 1, FirstName = "Béla", LastName = "Kovács", DateOfBirth = DateTime.ParseExact("19870224", "yyyyMMdd", null), DateOfJoin = DateTime.ParseExact("20190501", "yyyyMMdd", null), DateOfLeft = DateTime.ParseExact("20190901", "yyyyMMdd", null), Sex = "male", PhoneNumber = "305168813", Price = 3500 };
            Instructor i1 = new Instructor() { InstructorId = 2, FirstName = "Róbert", LastName = "Kiss", DateOfBirth = DateTime.ParseExact("19911015", "yyyyMMdd", null), DateOfJoin = DateTime.ParseExact("20190601", "yyyyMMdd", null), Sex = "male", PhoneNumber = "701987819", Price = 5000 };
            Instructor i2 = new Instructor() { InstructorId = 3, FirstName = "Ernő", LastName = "Horváth", DateOfBirth = DateTime.ParseExact("19930814", "yyyyMMdd", null), DateOfJoin = DateTime.ParseExact("20190511", "yyyyMMdd", null), Sex = "male", PhoneNumber = "702684468", Price = 4000 };

            Student s0 = new Student() { StudentId = 1, FirstName = "Ervin", LastName = "Szabó", DateOfBirth = DateTime.ParseExact("19960722", "yyyyMMdd", null), Sex = "male", PhoneNumber = "301987387", Email = "szervin@gmail.com" };
            Student s1 = new Student() { StudentId = 2, FirstName = "Csongor", LastName = "Molnár", DateOfBirth = DateTime.ParseExact("19940512", "yyyyMMdd", null), Sex = "male", PhoneNumber = "309889987", Email = "mcsongor@gmail.com" };
            Student s2 = new Student() { StudentId = 3, FirstName = "Eszter", LastName = "Supka", DateOfBirth = DateTime.ParseExact("19980624", "yyyyMMdd", null), Sex = "female", PhoneNumber = "309871398", Email = "seszter@gmail.com" };
            Student s3 = new Student() { StudentId = 4, FirstName = "Lilla", LastName = "Kiss", DateOfBirth = DateTime.ParseExact("19950817", "yyyyMMdd", null), Sex = "female", PhoneNumber = "709098779", Email = "klilla@gmail.com" };

            Car c0 = new Car() { CarId = 1, LicensePlate = "BIH-197", Brand = "Toyota", Type = "Corolla", Automatic = false, ManufacturingYear = 2008 };
            Car c1 = new Car() { CarId = 2, LicensePlate = "GJK-981", Brand = "Ford", Type = "Focus", Automatic = true, ManufacturingYear = 2016 };
            Car c2 = new Car() { CarId = 3, LicensePlate = "VGT-184", Brand = "Volkswagen", Type = "Golf", Automatic = false, ManufacturingYear = 2012 };

            Lesson l0 = new Lesson() { LessonId = 1, LessonDate = DateTime.ParseExact("20190511", "yyyyMMdd", null), LessonTime = 90, Distance = 37, InstructorId = 1, StudentId = 1, CarId = 1 };
            Lesson l1 = new Lesson() { LessonId = 2, LessonDate = DateTime.ParseExact("20190513", "yyyyMMdd", null), LessonTime = 120, Distance = 67, InstructorId = 2, StudentId = 2, CarId = 2 };
            Lesson l2 = new Lesson() { LessonId = 3, LessonDate = DateTime.ParseExact("20190515", "yyyyMMdd", null), LessonTime = 120, Distance = 58, InstructorId = 1, StudentId = 3, CarId = 1 };
            Lesson l3 = new Lesson() { LessonId = 4, LessonDate = DateTime.ParseExact("20190517", "yyyyMMdd", null), LessonTime = 90, Distance = 72, InstructorId = 2, StudentId = 3, CarId = 3 };
            Lesson l4 = new Lesson() { LessonId = 5, LessonDate = DateTime.ParseExact("20190520", "yyyyMMdd", null), LessonTime = 120, Distance = 47, InstructorId = 1, StudentId = 1, CarId = 1 };
            Lesson l5 = new Lesson() { LessonId = 6, LessonDate = DateTime.ParseExact("20190522", "yyyyMMdd", null), LessonTime = 90, Distance = 81, InstructorId = 2, StudentId = 4, CarId = 2 };
            Lesson l6 = new Lesson() { LessonId = 7, LessonDate = DateTime.ParseExact("20190526", "yyyyMMdd", null), LessonTime = 90, Distance = 55, InstructorId = 3, StudentId = 2, CarId = 1 };
            Lesson l7 = new Lesson() { LessonId = 8, LessonDate = DateTime.ParseExact("20190527", "yyyyMMdd", null), LessonTime = 120, Distance = 62, InstructorId = 2, StudentId = 4, CarId = 2 };
            Lesson l8 = new Lesson() { LessonId = 9, LessonDate = DateTime.ParseExact("20190529", "yyyyMMdd", null), LessonTime = 90, Distance = 38, InstructorId = 3, StudentId = 2, CarId = 3 };
            Lesson l9 = new Lesson() { LessonId = 10, LessonDate = DateTime.ParseExact("20190530", "yyyyMMdd", null), LessonTime = 90, Distance = 58, InstructorId = 2, StudentId = 4, CarId = 1 };

            modelBuilder?.Entity<Lesson>(entity =>
            {
                entity.HasOne(lessons => lessons.Instructor)
                .WithMany(instructors => instructors.Lesson)
                .HasForeignKey(lessons => lessons.InstructorId)
                .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(lessons => lessons.Student)
                .WithMany(students => students.Lesson)
                .HasForeignKey(lessons => lessons.StudentId)
                .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(lessons => lessons.Car)
                .WithMany(cars => cars.Lesson)
                .HasForeignKey(lessons => lessons.CarId)
                .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<Instructor>(entity =>
            {
                entity.HasMany(instructors => instructors.Lesson)
                .WithOne(lesson => lesson.Instructor)
                .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<Student>(entity =>
            {
                entity.HasMany(students => students.Lesson)
                .WithOne(lesson => lesson.Student)
                .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<Car>(entity =>
            {
                entity.HasMany(cars => cars.Lesson)
                .WithOne(lesson => lesson.Car)
                .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<Student>().HasData(s0, s1, s2, s3);
            modelBuilder.Entity<Car>().HasData(c0, c1, c2);
            modelBuilder.Entity<Instructor>().HasData(i0, i1, i2);
            modelBuilder.Entity<Lesson>().HasData(l0, l1, l2, l3, l4, l5, l6, l7, l8, l9);
        }
    }
}