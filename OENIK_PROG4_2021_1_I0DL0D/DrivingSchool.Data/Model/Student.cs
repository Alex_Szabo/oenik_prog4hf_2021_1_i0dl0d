﻿// <copyright file="Student.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrivingSchool.Data.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Reflection;
    using System.Text;

    /// <summary>
    /// Table of students.
    /// </summary>
    [Table("Students")]
    public class Student
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Student"/> class.
        /// Sets the lesson to be a HashSet.
        /// </summary>
        public Student()
        {
            this.Lesson = new HashSet<Lesson>();
        }

        /// <summary>
        /// Gets or sets student's id.
        /// </summary>
        [Key]
        [ToString]
        public int StudentId { get; set; }

        /// <summary>
        /// Gets or sets first name of student.
        /// </summary>
        [Required]
        [ToString]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets last name of student.
        /// </summary>
        [Required]
        [ToString]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets student's date of birth.
        /// </summary>
        [ToString]
        public DateTime DateOfBirth { get; set; }

        /// <summary>
        /// Gets or sets student's sex.
        /// </summary>
        [Required]
        [ToString]
        public string Sex { get; set; }

        /// <summary>
        /// Gets or sets student's phone number.
        /// </summary>
        [Required]
        [ToString]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Gets or sets student's email.
        /// </summary>
        [ToString]
        public string Email { get; set; }

        /// <summary>
        /// Gets the lesson of student.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Lesson> Lesson { get; }

        /// <summary>
        /// ToString method to write out all important elements with their values of this table.
        /// </summary>
        /// <returns>A string that has all elements with values of this table.</returns>
        public override string ToString()
        {
            string s = string.Empty;

            foreach (var item in this.GetType().GetProperties().Where(x => x.GetCustomAttribute<ToStringAttribute>() != null))
            {
                s += " ";
                s += item.Name + "\t=> ";
                s += item.GetValue(this);
                s += "\n";
            }

            s += " ";
            return s;
        }
    }
}
