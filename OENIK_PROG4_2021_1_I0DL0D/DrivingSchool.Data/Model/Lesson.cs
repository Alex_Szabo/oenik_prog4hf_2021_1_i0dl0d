﻿// <copyright file="Lesson.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrivingSchool.Data.Model
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using Microsoft.EntityFrameworkCore.Metadata.Internal;

    /// <summary>
    /// Table of lessons.
    /// </summary>
    [Table("Lessons")]
    public class Lesson
    {
        /// <summary>
        /// Gets or sets the lesson's id.
        /// </summary>
        [Key]
        [ToString]
        public int LessonId { get; set; }

        /// <summary>
        /// Gets or sets the lesson's date.
        /// </summary>
        [Required]
        [ToString]
        public DateTime LessonDate { get; set; }

        /// <summary>
        /// Gets or sets the lesson's duration.
        /// </summary>
        [Required]
        [ToString]
        public int LessonTime { get; set; }

        /// <summary>
        /// Gets or sets the lesson's distance.
        /// </summary>
        [Required]
        [ToString]
        public int Distance { get; set; }

        /// <summary>
        /// Gets or sets instructor's foreign key for lesson.
        /// </summary>
        [ToString]
        [ForeignKey(nameof(Instructor))]
        public int InstructorId { get; set; }

        /// <summary>
        /// Gets or sets student's foreign key for lesson.
        /// </summary>
        [ToString]
        [ForeignKey(nameof(Student))]
        public int StudentId { get; set; }

        /// <summary>
        /// Gets or sets car's foreign key for lesson.
        /// </summary>
        [ToString]
        [ForeignKey(nameof(Car))]
        public int CarId { get; set; }

        /// <summary>
        /// Gets or sets the instructors for the lesson table.
        /// </summary>
        public virtual Instructor Instructor { get; set; }

        /// <summary>
        /// Gets or sets the students for the lesson table.
        /// </summary>
        public virtual Student Student { get; set; }

        /// <summary>
        /// Gets or sets the cars for the lesson table.
        /// </summary>
        public virtual Car Car { get; set; }

        /// <summary>
        /// ToString method to write out all important elements with their values of this table.
        /// </summary>
        /// <returns>A string that has all elements with values of this table.</returns>
        public override string ToString()
        {
            string s = string.Empty;

            foreach (var item in this.GetType().GetProperties().Where(x => x.GetCustomAttribute<ToStringAttribute>() != null))
            {
                s += " ";
                s += item.Name + "\t=> ";
                s += item.GetValue(this);
                s += "\n";
            }

            s += " ";
            return s;
        }
    }
}
