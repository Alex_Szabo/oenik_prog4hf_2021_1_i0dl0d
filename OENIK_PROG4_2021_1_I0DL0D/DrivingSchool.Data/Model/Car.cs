﻿// <copyright file="Car.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrivingSchool.Data.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Reflection;
    using System.Text;

    /// <summary>
    /// Table of cars.
    /// </summary>
    [Table("Cars")]
    public class Car
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Car"/> class.
        /// Sets the lesson to be a HashSet.
        /// </summary>
        public Car()
        {
            this.Lesson = new HashSet<Lesson>();
        }

        /// <summary>
        /// Gets or sets the id of car.
        /// </summary>
        [Key]
        [ToString]
        public int CarId { get; set; }

        /// <summary>
        /// Gets or sets the license plate of car.
        /// </summary>
        [Required]
        [ToString]
        public string LicensePlate { get; set; }

        /// <summary>
        /// Gets or sets the brand of car.
        /// </summary>
        [ToString]
        public string Brand { get; set; }

        /// <summary>
        /// Gets or sets the type of car.
        /// </summary>
        [ToString]
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the car has manual or automatic transmission.
        /// </summary>
        [ToString]
        public bool Automatic { get; set; }

        /// <summary>
        /// Gets or sets the manufacturing year of car.
        /// </summary>
        [ToString]
        public int ManufacturingYear { get; set; }

        /// <summary>
        /// Gets the lesson of car.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Lesson> Lesson { get; }

        /// <summary>
        /// Method to write out all important elements with their values of this table.
        /// </summary>
        /// <returns>A string that has all important elements and values of this table.</returns>
        public override string ToString()
        {
            string s = string.Empty;

            foreach (var item in this.GetType().GetProperties().Where(x => x.GetCustomAttribute<ToStringAttribute>() != null))
            {
                s += " ";
                s += item.Name + "\t=> ";
                s += item.GetValue(this);
                s += "\n";
            }

            s += " ";
            return s;
        }

        /// <summary>
        /// Checks if the other object is the same type and their equality.
        /// </summary>
        /// <param name="obj">The other object.</param>
        /// <returns>True, if they are the same type and equal.</returns>
        public override bool Equals(object obj)
        {
            if (obj is Car)
            {
                Car other = obj as Car;
                return this.Brand == other.Brand &&
                    this.LicensePlate == other.LicensePlate &&
                    this.CarId == other.CarId &&
                    this.ManufacturingYear == other.ManufacturingYear;
            }

            return false;
        }

        /// <summary>
        /// Generates object specific HashCode.
        /// </summary>
        /// <returns>Object specific integer.</returns>
        public override int GetHashCode()
        {
            return this.ManufacturingYear * this.Brand.Length * this.CarId;
        }
    }
}
