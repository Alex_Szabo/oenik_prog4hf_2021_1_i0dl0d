﻿// <copyright file="IStudentRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Driving.School.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using DrivingSchool.Data.Model;

    /// <summary>
    /// Interface for StudentRepository methods.
    /// </summary>
    public interface IStudentRepository : IRepository<Student>
    {
        /// <summary>
        /// Changes the Email of a Student.
        /// </summary>
        /// <param name="id">Id of a Student.</param>
        /// <param name="newEmail">New Email for Student.</param>
        void ChangeEmail(int id, string newEmail);

        /// <summary>
        /// Changes the FirstName of a Student.
        /// </summary>
        /// <param name="id">Id of a Student.</param>
        /// <param name="newFirstName">New FirstName for Student.</param>
        void ChangeFirstName(int id, string newFirstName);

        /// <summary>
        /// Changes the LastName of a Student.
        /// </summary>
        /// <param name="id">Id of a Student.</param>
        /// <param name="newLastName">New LastName for Student.</param>
        void ChangeLastName(int id, string newLastName);

        /// <summary>
        /// Changes the Sex of a Student.
        /// </summary>
        /// <param name="id">Id of a Student.</param>
        /// <param name="newSex">New Sex for Student.</param>
        void ChangeSex(int id, string newSex);

        /// <summary>
        /// Changes the PhoneNumber of a Student.
        /// </summary>
        /// <param name="id">Id of a Student.</param>
        /// <param name="newPhoneNumber">New PhoneNumber for Student.</param>
        void ChangePhoneNumber(int id, string newPhoneNumber);

        /// <summary>
        /// Changes the DOB of a Student.
        /// </summary>
        /// <param name="id">Id of a Student.</param>
        /// <param name="newDOB">New DOB for Student.</param>
        void ChangeDOB(int id, DateTime newDOB);
    }
}
