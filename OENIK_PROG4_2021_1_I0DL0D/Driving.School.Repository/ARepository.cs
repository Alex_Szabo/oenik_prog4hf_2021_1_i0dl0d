﻿// <copyright file="ARepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Driving.School.Repository
{
    using System;
    using System.Linq;
    using DrivingSchool.Data.Model;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Abstract class with CRD and GetOne element for DatabaseContext.
    /// </summary>
    /// <typeparam name="T">
    /// T generic type for use of every inherited type.
    /// </typeparam>
    public abstract class ARepository<T> : IRepository<T>
        where T : class
    {
        /// <summary>
        /// Private field that uses DatabaseContext.
        /// </summary>
        private DbContext ctx;

        /// <summary>
        /// Initializes a new instance of the <see cref="ARepository{T}"/> class.
        /// </summary>
        /// <param name="ctx">
        /// A DatabaseContext database.
        /// </param>
        protected ARepository(DbContext ctx)
        {
            this.ctx = ctx;
        }

        /// <summary>
        /// Gets a private DatabaseContext and makes it available for inherited classes.
        /// </summary>
        protected DbContext Ctx
        {
            get
            {
                return this.ctx;
            }
        }

        /// <summary>
        /// Method that gets all elements of a type in database.
        /// </summary>
        /// <returns>Set of all elements.</returns>
        public IQueryable<T> GetAll()
        {
            return this.Ctx.Set<T>();
        }

        /// <summary>
        /// Method that will return an element by id.
        /// </summary>
        /// <param name="id">Id of element.</param>
        /// <returns>Element with the Id.</returns>
        public abstract T GetOne(int id);

        /// <summary>
        /// Method for add new element to the database.
        /// </summary>
        /// <param name="newInstance">A new object for the database.</param>
        public void Add(T newInstance)
        {
            this.Ctx.Add(newInstance);
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// Remove method for removing an element from database.
        /// </summary>
        /// <param name="id">The id of the removable element.</param>
        public void Remove(int id)
        {
             this.Ctx.Remove(this.GetOne(id));
             this.Ctx.SaveChanges();
        }
    }
}
