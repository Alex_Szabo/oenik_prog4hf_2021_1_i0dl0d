﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Driving.School.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DrivingSchool.Data.Model;

    /// <summary>
    /// Interface for ARepository methods.
    /// </summary>
    /// <typeparam name="T">Generic type for inherited objects.</typeparam>
    public interface IRepository<T>
        where T : class
    {
        /// <summary>
        /// Gets an element from an Id.
        /// </summary>
        /// <param name="id">Id of element.</param>
        /// <returns>Element with the Id.</returns>
        T GetOne(int id);

        /// <summary>
        /// Get all element method for an object.
        /// </summary>
        /// <returns>All elements of an object.</returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// Add new element to an object.
        /// </summary>
        /// <param name="newInstance">A new element.</param>
        void Add(T newInstance);

        /// <summary>
        /// Remove an element by Id.
        /// </summary>
        /// <param name="id">Id of element.</param>
        void Remove(int id);
    }
}
