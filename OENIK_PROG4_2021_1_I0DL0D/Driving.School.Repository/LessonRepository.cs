﻿// <copyright file="LessonRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Driving.School.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DrivingSchool.Data.Model;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Makes Lesson elements accessible from higher layers.
    /// </summary>
    public class LessonRepository : ARepository<Lesson>, ILessonRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LessonRepository"/> class.
        /// </summary>
        /// <param name="ctx">A DatabaseContext.</param>
        public LessonRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// Changes the Distance of a Lesson.
        /// </summary>
        /// <param name="id">Id of Lesson.</param>
        /// <param name="newDistance">New Distance for Lesson.</param>
        public void ChangeDistance(int id, int newDistance)
        {
            var lesson = this.GetOne(id);
            if (lesson != null)
            {
                lesson.Distance = newDistance;
                this.Ctx.SaveChanges();
            }
            else
            {
                throw new InvalidOperationException();
            }
        }

        /// <summary>
        /// Gets a Lesson element from Id.
        /// </summary>
        /// <param name="id">Id of Lesson.</param>
        /// <returns>Lesson element with the Id.</returns>
        public override Lesson GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.LessonId == id);
        }
    }
}
