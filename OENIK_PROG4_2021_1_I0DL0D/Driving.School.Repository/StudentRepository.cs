﻿// <copyright file="StudentRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Driving.School.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DrivingSchool.Data.Model;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Makes Student elements accessible from higher layers.
    /// </summary>
    public class StudentRepository : ARepository<Student>, IStudentRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StudentRepository"/> class.
        /// </summary>
        /// <param name="ctx">A DatabaseContext.</param>
        public StudentRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// Changes the Email of a Student.
        /// </summary>
        /// <param name="id">Id of a Student.</param>
        /// <param name="newEmail">New Email for Student.</param>
        public void ChangeEmail(int id, string newEmail)
        {
            var student = this.GetOne(id);
            if (student != null)
            {
                student.Email = newEmail;
                this.Ctx.SaveChanges();
            }
            else
            {
                throw new InvalidOperationException();
            }
        }

        /// <summary>
        /// Changes the FirstName of a Student.
        /// </summary>
        /// <param name="id">Id of a Student.</param>
        /// <param name="newFirstName">New FirstName for Student.</param>
        public void ChangeFirstName(int id, string newFirstName)
        {
            var student = this.GetOne(id);
            if (student != null)
            {
                student.FirstName = newFirstName;
                this.Ctx.SaveChanges();
            }
            else
            {
                throw new InvalidOperationException();
            }
        }

        /// <summary>
        /// Changes the LastName of a Student.
        /// </summary>
        /// <param name="id">Id of a Student.</param>
        /// <param name="newLastName">New LastName for Student.</param>
        public void ChangeLastName(int id, string newLastName)
        {
            var student = this.GetOne(id);
            if (student != null)
            {
                student.LastName = newLastName;
                this.Ctx.SaveChanges();
            }
            else
            {
                throw new InvalidOperationException();
            }
        }

        /// <summary>
        /// Changes the PhoneNumber of a Student.
        /// </summary>
        /// <param name="id">Id of a Student.</param>
        /// <param name="newPhoneNumber">New PhoneNumber for Student.</param>
        public void ChangePhoneNumber(int id, string newPhoneNumber)
        {
            var student = this.GetOne(id);
            if (student != null)
            {
                student.PhoneNumber = newPhoneNumber;
                this.Ctx.SaveChanges();
            }
            else
            {
                throw new InvalidOperationException();
            }
        }

        /// <summary>
        /// Changes the Sex of a Student.
        /// </summary>
        /// <param name="id">Id of a Student.</param>
        /// <param name="newSex">New Sex for Student.</param>
        public void ChangeSex(int id, string newSex)
        {
            var student = this.GetOne(id);
            if (student != null)
            {
                student.Sex = newSex;
                this.Ctx.SaveChanges();
            }
            else
            {
                throw new InvalidOperationException();
            }
        }

        /// <summary>
        /// Changes the DOB of a Student.
        /// </summary>
        /// <param name="id">Id of a Student.</param>
        /// <param name="newDOB">New DOB for Student.</param>
        public void ChangeDOB(int id, DateTime newDOB)
        {
            var student = this.GetOne(id);
            if (student != null)
            {
                student.DateOfBirth = newDOB;
                this.Ctx.SaveChanges();
            }
            else
            {
                throw new InvalidOperationException();
            }
        }

        /// <summary>
        /// Gets a Student element from Id.
        /// </summary>
        /// <param name="id">Id of Student.</param>
        /// <returns>Student element with the Id.</returns>
        public override Student GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.StudentId == id);
        }
    }
}
