﻿// <copyright file="InstructorRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Driving.School.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DrivingSchool.Data.Model;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Makes the Instructor table accessible from higher layer.
    /// </summary>
    public class InstructorRepository : ARepository<Instructor>, IInstructorRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InstructorRepository"/> class.
        /// </summary>
        /// <param name="ctx">A DatabaseContext.</param>
        public InstructorRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// Changes the Price for an Instructor.
        /// </summary>
        /// <param name="id">Id of Instructor.</param>
        /// <param name="newPrice">New Price for Instructor.</param>
        public void ChangePrice(int id, int newPrice)
        {
            var instructor = this.GetOne(id);
            if (instructor != null)
            {
                instructor.Price = newPrice;
                this.Ctx.SaveChanges();
            }
            else
            {
                throw new InvalidOperationException();
            }
        }

        /// <summary>
        /// Gets an Instructor from Id.
        /// </summary>
        /// <param name="id">Id of Instructor.</param>
        /// <returns>Instructor element.</returns>
        public override Instructor GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.InstructorId == id);
        }
    }
}
