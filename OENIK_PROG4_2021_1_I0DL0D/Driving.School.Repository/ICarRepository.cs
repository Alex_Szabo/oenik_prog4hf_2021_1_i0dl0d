﻿// <copyright file="ICarRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Driving.School.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using DrivingSchool.Data.Model;

    /// <summary>
    /// Interface for CarRepository methods.
    /// </summary>
    public interface ICarRepository : IRepository<Car>
    {
        /// <summary>
        /// Changes the LicensePlate of a Car.
        /// </summary>
        /// <param name="id">Id of the Car.</param>
        /// <param name="newLicensePlate">The new LicensePlate.</param>
        void ChangeLicensePlate(int id, string newLicensePlate);
    }
}
