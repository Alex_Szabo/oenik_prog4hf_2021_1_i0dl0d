﻿// <copyright file="IInstructorRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Driving.School.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using DrivingSchool.Data.Model;

    /// <summary>
    /// Interface for InstructorRepository methods.
    /// </summary>
    public interface IInstructorRepository : IRepository<Instructor>
    {
        /// <summary>
        /// Changes the price for an Instructor.
        /// </summary>
        /// <param name="id">Id of Instructor.</param>
        /// <param name="newPrice">New price for Instructor.</param>
        void ChangePrice(int id, int newPrice);
    }
}
