﻿// <copyright file="ILessonRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Driving.School.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using DrivingSchool.Data.Model;

    /// <summary>
    /// Interface for LessonRepository methods.
    /// </summary>
    public interface ILessonRepository : IRepository<Lesson>
    {
        /// <summary>
        /// Changes the Distance of a Lesson.
        /// </summary>
        /// <param name="id">Id of lesson.</param>
        /// <param name="newDistance">New distance for lesson.</param>
        void ChangeDistance(int id, int newDistance);
    }
}
