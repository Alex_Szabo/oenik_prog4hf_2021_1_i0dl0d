﻿// <copyright file="CarRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Driving.School.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DrivingSchool.Data.Model;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Makes the Car table accessible from higher layers.
    /// </summary>
    public class CarRepository : ARepository<Car>, ICarRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CarRepository"/> class.
        /// </summary>
        /// <param name="ctx">A DatabaseContext object.</param>
        public CarRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// Changes the LicensePlate of an element.
        /// </summary>
        /// <param name="id">Id of the target element.</param>
        /// <param name="newLicensePlate">The new LicensePlate value.</param>
        public void ChangeLicensePlate(int id, string newLicensePlate)
        {
            var car = this.GetOne(id);
            if (car != null)
            {
                car.LicensePlate = newLicensePlate;
                this.Ctx.SaveChanges();
            }
            else
            {
                throw new InvalidOperationException();
            }
        }

        /// <summary>
        /// Gets a Car element by id.
        /// </summary>
        /// <param name="id">Id of the Car.</param>
        /// <returns>Car element.</returns>
        public override Car GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.CarId == id);
        }
    }
}
