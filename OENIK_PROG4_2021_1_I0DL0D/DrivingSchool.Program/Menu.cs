﻿// <copyright file="Menu.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrivingSchool.Program
{
    using System;
    using System.Globalization;
    using System.Linq;
    using ConsoleTools;
    using DrivingSchool.Data.Model;
    using DrivingSchool.Logic;

    /// <summary>
    /// Console Menu of the program.
    /// </summary>
    public class Menu
    {
        /// <summary>
        /// Initializes Factory and Console Menu.
        /// </summary>
        /// <param name="args">CommandLine arguments.</param>
        private static void Main(string[] args)
        {
            Factory factory = new Factory();

            var listmenu = new ConsoleMenu(args, level: 1)
                .Add(">> List all instructors", () => ListAllInstructors(factory.StaffLogic))
                .Add(">> List all students", () => ListAllStudents(factory.StudentsDrivingLogic))
                .Add(">> List all cars", () => ListAllCars(factory.VehicleUsageLogic))
                .Add(">> List all lessons", () => ListAllLessons(factory.StudentsDrivingLogic))
                .Add(">> Main menu", ConsoleMenu.Close);

            var createmenu = new ConsoleMenu(args, level: 1)
                .Add(">> Add new instructor", () => AddNewInstructor(factory.StaffLogic))
                .Add(">> Add new student", () => AddNewStudent(factory.StudentsDrivingLogic))
                .Add(">> Add new car", () => AddNewCar(factory.VehicleUsageLogic))
                .Add(">> Add new lesson", () => AddNewLesson(factory.StudentsDrivingLogic))
                .Add(">> Main menu", ConsoleMenu.Close);

            var deletemenu = new ConsoleMenu(args, level: 1)
                .Add(">> Remove an instructor by id", () => RemoveInstructor(factory.StaffLogic))
                .Add(">> Remove a student by id", () => RemoveStudent(factory.StudentsDrivingLogic))
                .Add(">> Remove a car by id", () => RemoveCar(factory.VehicleUsageLogic))
                .Add(">> Remove a lesson by id", () => RemoveLesson(factory.StudentsDrivingLogic))
                .Add(">> Main menu", ConsoleMenu.Close);

            var updatemenu = new ConsoleMenu(args, level: 1)
                .Add(">> Update an instructor's price", () => ChangeInstructorPrice(factory.StaffLogic))
                .Add(">> Update a car's license plate", () => ChangeLicensePlate(factory.VehicleUsageLogic))
                .Add(">> Update a student's email address", () => ChangeEmail(factory.StudentsDrivingLogic))
                .Add(">> Update a lesson's distance", () => LessonDistance(factory.StudentsDrivingLogic))
                .Add(">> Main menu", ConsoleMenu.Close);

            var queriesmenu = new ConsoleMenu(args, level: 1)
                .Add(">> Average speed by sex", () => AverageSpeedBySex(factory.StudentsDrivingLogic))
                .Add(">> List instructors by total payment", () => TotalPaymentByInstructor(factory.StaffLogic))
                .Add(">> List cars by total distance", () => TotalDistanceByCar(factory.VehicleUsageLogic))
                .Add(">> Average speed by sex with task", () => AverageSpeedBySexTask(factory.StudentsDrivingLogic))
                .Add(">> List cars by total distance with task", () => TotalDistanceByCarTask(factory.VehicleUsageLogic))
                .Add(">> List instructors by total payment with task", () => TotalPaymentByInstructorTask(factory.StaffLogic))
                .Add(">> Main menu", ConsoleMenu.Close);

            var menu = new ConsoleMenu(args, level: 0)
                .Add(">> List data", listmenu.Show)
                .Add(">> Add new element", createmenu.Show)
                .Add(">> Delete an element", deletemenu.Show)
                .Add(">> Update an element", updatemenu.Show)
                .Add(">> Queries", queriesmenu.Show)
                .Add(">> Exit", ConsoleMenu.Close);

            menu.Show();
            factory.DrivingSchoolContext.Dispose();
        }

        /// <summary>
        /// Writes out all Instructors to console.
        /// </summary>
        /// <param name="staffLogic">StaffLogic Logic.</param>
        private static void ListAllInstructors(StaffLogic staffLogic)
        {
            staffLogic.GetAllInstructors().ToList().ForEach(x => Console.WriteLine(x.ToString()));
            Console.ReadLine();
        }

        /// <summary>
        /// Writes out all Students to console.
        /// </summary>
        /// <param name="studentsDrivingLogic">StudentsDrivingLogic Logic.</param>
        private static void ListAllStudents(StudentsDrivingLogic studentsDrivingLogic)
        {
            studentsDrivingLogic.GetAllStudents().ToList().ForEach(x => Console.WriteLine(x.ToString()));

            Console.ReadLine();
        }

        /// <summary>
        /// Writes out all Cars to console.
        /// </summary>
        /// <param name="vehicleUsageLogic">VehicleUsageLogic Logic.</param>
        private static void ListAllCars(VehicleUsageLogic vehicleUsageLogic)
        {
            vehicleUsageLogic.GetAllCars().ToList().ForEach(x => Console.WriteLine(x.ToString()));

            Console.ReadLine();
        }

        /// <summary>
        /// Writes out all Lessons to console.
        /// </summary>
        /// <param name="studentsDrivingLogic">StudentsDrivingLogic Logic.</param>
        private static void ListAllLessons(StudentsDrivingLogic studentsDrivingLogic)
        {
            studentsDrivingLogic.GetAllLessons().ToList().ForEach(x => Console.WriteLine(x.ToString()));

            Console.ReadLine();
        }

        /// <summary>
        /// Adds new Instructor from console.
        /// </summary>
        /// <param name="staffLogic">StaffLogic Logic.</param>
        private static void AddNewInstructor(StaffLogic staffLogic)
        {
            try
            {
                Console.Write("First Name: ");
                string firstName = Console.ReadLine();
                Console.Write("Last Name: ");
                string lastName = Console.ReadLine();
                Console.Write("Date of Birth (yyyy.mm.dd.): ");
                string dob = Console.ReadLine();
                Console.Write("Date of Join (yyyy.mm.dd.): ");
                string doj = Console.ReadLine();
                Console.Write("Price: ");
                int price = int.Parse(Console.ReadLine(), CultureInfo.CurrentCulture);
                Console.Write("Sex (male/female): ");
                string sex = Console.ReadLine();
                Console.Write("Phone number: ");
                string phonenumber = Console.ReadLine();
                staffLogic.AddInstructor(firstName, lastName, dob, doj, price, sex, phonenumber);
                TextWriter("Add successful! \n");
                Console.ReadLine();
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }
        }

        /// <summary>
        /// Adds new Student from console.
        /// </summary>
        /// <param name="studentsDrivingLogic">StudentsDrivingLogic Logic.</param>
        private static void AddNewStudent(StudentsDrivingLogic studentsDrivingLogic)
        {
            try
            {
                Console.Write("First Name: ");
                string firstName = Console.ReadLine();
                Console.Write("Last Name: ");
                string lastName = Console.ReadLine();
                Console.Write("Date of Birth (yyyy.mm.dd.): ");
                string dob = Console.ReadLine();
                Console.Write("Sex (male/female): ");
                string sex = Console.ReadLine();
                Console.Write("Email address: ");
                string email = Console.ReadLine();
                Console.Write("Phone number: ");
                string phone = Console.ReadLine();
                studentsDrivingLogic.AddStudent(firstName, lastName, dob, email, phone, sex);
                TextWriter("Add successful! \n");
                Console.ReadLine();
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }
        }

        /// <summary>
        /// Adds new Car from console.
        /// </summary>
        /// <param name="vehicleUsageLogic">VehicleUsageLogic Logic.</param>
        private static void AddNewCar(VehicleUsageLogic vehicleUsageLogic)
        {
            try
            {
                Console.Write("Brand: ");
                string brand = Console.ReadLine();
                Console.Write("Type: ");
                string type = Console.ReadLine();
                Console.Write("Manufacturing Year: ");
                int manufacturingyear = int.Parse(Console.ReadLine(), CultureInfo.CurrentCulture);
                Console.Write("License Plate: ");
                string licenseplate = Console.ReadLine();
                Console.Write("Transmission type (auto/manual): ");
                string transmissions = Console.ReadLine();
                bool transmission = false;
                if (transmissions == "auto")
                {
                    transmission = true;
                }

                vehicleUsageLogic.AddCar(brand, type, manufacturingyear, licenseplate, transmission);
                TextWriter("Add successful! \n");

                Console.ReadLine();
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }
        }

        /// <summary>
        /// Adds new Lesson from console.
        /// </summary>
        /// <param name="studentsDrivingLogic">StudentsDrivingLogic Logic.</param>
        private static void AddNewLesson(StudentsDrivingLogic studentsDrivingLogic)
        {
            try
            {
                Console.Write("Lesson's Date: ");
                string lessondate = Console.ReadLine();
                Console.Write("Lesson's Time: ");
                int lessontime = int.Parse(Console.ReadLine(), CultureInfo.CurrentCulture);
                Console.Write("Distance: ");
                int distance = int.Parse(Console.ReadLine(), CultureInfo.CurrentCulture);
                Console.Write("Instructor Id: ");
                int instructorid = int.Parse(Console.ReadLine(), CultureInfo.CurrentCulture);
                Console.Write("Student Id: ");
                int studentid = int.Parse(Console.ReadLine(), CultureInfo.CurrentCulture);
                Console.Write("Car Id: ");
                int carid = int.Parse(Console.ReadLine(), CultureInfo.CurrentCulture);
                try
                {
                    studentsDrivingLogic.AddLesson(lessondate, lessontime, distance, instructorid, studentid, carid);
                    TextWriter("Add successful! \n");
                    Console.ReadLine();
                }
                catch (Microsoft.EntityFrameworkCore.DbUpdateException)
                {
                    TextWriter("One of the entered id's do not exist!" + "\n");
                    Console.ReadLine();
                }
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }
        }

        /// <summary>
        /// Writes out average speed by sex to console.
        /// </summary>
        /// <param name="studentsDrivingLogic">StudentsDrivingLogic Logic.</param>
        private static void AverageSpeedBySex(StudentsDrivingLogic studentsDrivingLogic)
        {
            var q = studentsDrivingLogic.AverageSpeedBySex();
            foreach (var item in q)
            {
                Console.WriteLine("Sex: " + item.Sex + " - " + "Average speed: " + item.AverageSpeed + " km/h");
            }

            Console.ReadLine();
        }

        /// <summary>
        /// Writes out average speed by sex to console.
        /// </summary>
        /// <param name="studentsDrivingLogic">StudentsDrivingLogic Logic.</param>
        private static void AverageSpeedBySexTask(StudentsDrivingLogic studentsDrivingLogic)
        {
            var task = studentsDrivingLogic.AverageSpeedBySexTask();
            task.Wait();
            var result = task.Result;

            foreach (var item in result)
            {
                Console.WriteLine("Sex: " + item.Sex + " - " + "Average speed: " + item.AverageSpeed + " km/h");
            }

            Console.ReadLine();
        }

        /// <summary>
        /// Writes out total distances by cars to console.
        /// </summary>
        /// <param name="vehicleUsageLogic">VehicleUsageLogic Logic.</param>
        private static void TotalDistanceByCar(VehicleUsageLogic vehicleUsageLogic)
        {
            var q = vehicleUsageLogic.TotalDistances();
            foreach (var item in q)
            {
                Console.WriteLine("Car's id: " + item.CarId + " - " + "Total distance: " + item.TotalDistance + " km");
            }

            Console.ReadLine();
        }

        /// <summary>
        /// Writes out total distances by cars to console.
        /// </summary>
        /// <param name="vehicleUsageLogic">VehicleUsageLogic Logic.</param>
        private static void TotalDistanceByCarTask(VehicleUsageLogic vehicleUsageLogic)
        {
            var task = vehicleUsageLogic.TotalDistancesTask();
            task.Wait();
            var result = task.Result;

            foreach (var item in result)
            {
                Console.WriteLine("Car's id: " + item.CarId + " - " + "Total distance: " + item.TotalDistance + " km");
            }

            Console.ReadLine();
        }

        /// <summary>
        /// Writes out total payments by instructors to console.
        /// </summary>
        /// <param name="staffLogic">StaffLogic Logic.</param>
        private static void TotalPaymentByInstructor(StaffLogic staffLogic)
        {
            var q = staffLogic.TotalPayments();
            foreach (var item in q)
            {
                Console.WriteLine("Instructor's id: " + item.InstructorId + " - " + "Total payment: " + item.TotalPayment + " Ft");
            }

            Console.ReadLine();
        }

        /// <summary>
        /// Writes out total payments by instructors to console.
        /// </summary>
        /// <param name="staffLogic">StaffLogic Logic.</param>
        private static void TotalPaymentByInstructorTask(StaffLogic staffLogic)
        {
            var task = staffLogic.TotalPaymentsTask();
            task.Wait();
            var result = task.Result;

            foreach (var item in result)
            {
                Console.WriteLine("Instructor's id: " + item.InstructorId + " - " + "Total payment: " + item.TotalPayment + " Ft");
            }

            Console.ReadLine();
        }

        /// <summary>
        /// Changes instructor's price by user input.
        /// </summary>
        /// <param name="staffLogic">StaffLogic Logic.</param>
        private static void ChangeInstructorPrice(StaffLogic staffLogic)
        {
            try
            {
                TextWriter("Instructor's id: ");

                int id = int.Parse(Console.ReadLine(), CultureInfo.CurrentCulture);

                TextWriter("Instructor's new price: ");

                int price = int.Parse(Console.ReadLine(), CultureInfo.CurrentCulture);

                try
                {
                    staffLogic.ChangeInstructorPrice(id, price);
                    TextWriter("Change successful! \n");
                    Console.ReadLine();
                }
                catch (InvalidOperationException)
                {
                    TextWriter("There is no instructor with this id!" + "\n");
                    Console.ReadLine();
                }
                catch (ArgumentNullException)
                {
                    TextWriter("There is no instructor with this id!" + "\n");
                    Console.ReadLine();
                }
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }
        }

        /// <summary>
        /// Changes car's license plate by user input.
        /// </summary>
        /// <param name="vehicleUsageLogic">VehicleUsageLogic Logic.</param>
        private static void ChangeLicensePlate(VehicleUsageLogic vehicleUsageLogic)
        {
            try
            {
                TextWriter("Car's id: ");
                int id = int.Parse(Console.ReadLine(), CultureInfo.CurrentCulture);

                TextWriter("Car's new license plate: ");

                string plate = Console.ReadLine();
                try
                {
                    vehicleUsageLogic.ChangeLicensePlate(id, plate);
                    TextWriter("Change successful! \n");
                    Console.ReadLine();
                }
                catch (InvalidOperationException)
                {
                    TextWriter("There is no car with this id!" + "\n");
                    Console.ReadLine();
                }
                catch (NullReferenceException)
                {
                    TextWriter("There is no car with this id!" + "\n");
                    Console.ReadLine();
                }
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }
        }

        /// <summary>
        /// Changes student's email by user input.
        /// </summary>
        /// <param name="studentsDrivingLogic">StudentsDrivingLogic Logic.</param>
        private static void ChangeEmail(StudentsDrivingLogic studentsDrivingLogic)
        {
            try
            {
                TextWriter("Student's id: ");
                int id = int.Parse(Console.ReadLine(), CultureInfo.CurrentCulture);

                TextWriter("Student's new email address: ");

                string email = Console.ReadLine();
                try
                {
                    studentsDrivingLogic.ChangeEmail(id, email);
                    TextWriter("Change successful! \n");
                    Console.ReadLine();
                }
                catch (InvalidOperationException)
                {
                    TextWriter("There is no student with this id!" + "\n");
                    Console.ReadLine();
                }
                catch (NullReferenceException)
                {
                    TextWriter("There is no student with this id!" + "\n");
                    Console.ReadLine();
                }
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }
        }

        /// <summary>
        /// Changes lesson's distance by user input.
        /// </summary>
        /// <param name="studentsDrivingLogic">StudentsDrivingLogic Logic.</param>
        private static void LessonDistance(StudentsDrivingLogic studentsDrivingLogic)
        {
            try
            {
                TextWriter("Lesson's id: ");
                int id = int.Parse(Console.ReadLine(), CultureInfo.CurrentCulture);

                TextWriter("Lesson's new distance: ");

                int distance = int.Parse(Console.ReadLine(), CultureInfo.CurrentCulture);
                try
                {
                    studentsDrivingLogic.ChangeDistance(id, distance);
                    TextWriter("Change successful! \n");
                    Console.ReadLine();
                }
                catch (InvalidOperationException)
                {
                    TextWriter("There is no lesson with this id!" + "\n");
                    Console.ReadLine();
                }
                catch (NullReferenceException)
                {
                    TextWriter("There is no lesson with this id!" + "\n");
                    Console.ReadLine();
                }
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }
        }

        /// <summary>
        /// Remove an instructor by user input.
        /// </summary>
        /// <param name="staffLogic">StaffLogic Logic.</param>
        private static void RemoveInstructor(StaffLogic staffLogic)
        {
            try
            {
                TextWriter("Instructor's id: ");
                int id = int.Parse(Console.ReadLine(), CultureInfo.CurrentCulture);

                try
                {
                    staffLogic.RemoveInstructor(id);
                    TextWriter("Remove successful! \n");
                    Console.ReadLine();
                }
                catch (NullReferenceException)
                {
                    TextWriter("There is no instructor with this id!" + "\n");
                    Console.ReadLine();
                }
                catch (ArgumentNullException)
                {
                    TextWriter("There is no instructor with this id!" + "\n");
                    Console.ReadLine();
                }
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }
        }

        /// <summary>
        /// Remove a student by user input.
        /// </summary>
        /// <param name="studentsDrivingLogic">StudentsDrivingLogic Logic.</param>
        private static void RemoveStudent(StudentsDrivingLogic studentsDrivingLogic)
        {
            try
            {
                TextWriter("Student's id: ");
                int id = int.Parse(Console.ReadLine(), CultureInfo.CurrentCulture);

                try
                {
                    studentsDrivingLogic.RemoveStudent(id);
                    TextWriter("Remove successful! \n");
                    Console.ReadLine();
                }
                catch (NullReferenceException)
                {
                    TextWriter("There is no student with this id!" + "\n");
                    Console.ReadLine();
                }
                catch (ArgumentNullException)
                {
                    TextWriter("There is no student with this id!" + "\n");
                    Console.ReadLine();
                }
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }
        }

        /// <summary>
        /// Remove a lesson by user input.
        /// </summary>
        /// <param name="studentsDrivingLogic">StudentsDrivingLogic Logic.</param>
        private static void RemoveLesson(StudentsDrivingLogic studentsDrivingLogic)
        {
            try
            {
                TextWriter("Lesson's id: ");
                int id = int.Parse(Console.ReadLine(), CultureInfo.CurrentCulture);

                try
                {
                    TextWriter("Remove successful! \n");
                    Console.ReadLine();
                    studentsDrivingLogic.RemoveLesson(id);
                }
                catch (NullReferenceException)
                {
                    TextWriter("There is no lesson with this id!" + "\n");
                    Console.ReadLine();
                }
                catch (ArgumentNullException)
                {
                    TextWriter("There is no lesson with this id!" + "\n");
                    Console.ReadLine();
                }
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }
        }

        /// <summary>
        /// Remove a car by user input.
        /// </summary>
        /// <param name="vehicleUsageLogic">VehicleUsageLogic Logic.</param>
        private static void RemoveCar(VehicleUsageLogic vehicleUsageLogic)
        {
            try
            {
                TextWriter("Car's id: ");
                int id = int.Parse(Console.ReadLine(), CultureInfo.CurrentCulture);

                try
                {
                    vehicleUsageLogic.RemoveCar(id);
                    TextWriter("Remove successful! \n");
                    Console.ReadLine();
                }
                catch (NullReferenceException)
                {
                    TextWriter("There is no car with this id!" + "\n");
                    Console.ReadLine();
                }
                catch (ArgumentNullException)
                {
                    TextWriter("There is no car with this id!" + "\n");
                    Console.ReadLine();
                }
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }
        }

        /// <summary>
        /// Gets a string and writes it out to console.
        /// </summary>
        /// <param name="s">A string to write out.</param>
        private static void TextWriter(string s)
        {
            Console.Write(s);
        }
    }
}
