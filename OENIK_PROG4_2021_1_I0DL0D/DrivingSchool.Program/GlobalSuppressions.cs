﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<Only used once.>", Scope = "member", Target = "~M:DrivingSchool.Program.Menu.AddNewStudent(DrivingSchool.Logic.StudentsDrivingLogic)")]
[assembly: SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<Only used once.>", Scope = "member", Target = "~M:DrivingSchool.Program.Menu.AddNewInstructor(DrivingSchool.Logic.StaffLogic)")]
[assembly: SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<Only used once.>", Scope = "member", Target = "~M:DrivingSchool.Program.Menu.AddNewCar(DrivingSchool.Logic.VehicleUsageLogic)")]
[assembly: SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<Only used once.>", Scope = "member", Target = "~M:DrivingSchool.Program.Menu.AddNewLesson(DrivingSchool.Logic.StudentsDrivingLogic)")]
