﻿// <copyright file="Factory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrivingSchool.Program
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Text;
    using Driving.School.Repository;
    using DrivingSchool.Data.Model;
    using DrivingSchool.Logic;

    /// <summary>
    /// Factory class to initialize class libraries.
    /// </summary>
    public class Factory
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Factory"/> class.
        /// Method to initialize library objects.
        /// </summary>
        public Factory()
        {
            this.DrivingSchoolContext = new DrivingSchoolContext();

            InstructorRepository instructorRepo = new InstructorRepository(this.DrivingSchoolContext);
            StudentRepository studentRepository = new StudentRepository(this.DrivingSchoolContext);
            CarRepository carRepository = new CarRepository(this.DrivingSchoolContext);
            LessonRepository lessonRepository = new LessonRepository(this.DrivingSchoolContext);

            StaffLogic staffLogic = new StaffLogic(instructorRepo, lessonRepository);
            StudentsDrivingLogic studentsDrivingLogic = new StudentsDrivingLogic(studentRepository, lessonRepository);
            VehicleUsageLogic vehicleUsageLogic = new VehicleUsageLogic(carRepository, lessonRepository);

            this.StaffLogic = staffLogic;
            this.StudentsDrivingLogic = studentsDrivingLogic;
            this.VehicleUsageLogic = vehicleUsageLogic;
        }

        /// <summary>
        /// Gets or sets StaffLogic.
        /// </summary>
        public StaffLogic StaffLogic { get; set; }

        /// <summary>
        /// Gets or sets StudentsDrivingLogic.
        /// </summary>
        public StudentsDrivingLogic StudentsDrivingLogic { get; set; }

        /// <summary>
        /// Gets or sets VehicleUsageLogic.
        /// </summary>
        public VehicleUsageLogic VehicleUsageLogic { get; set; }

        /// <summary>
        /// Gets or sets DrivingSchoolContext Database.
        /// </summary>
        public DrivingSchoolContext DrivingSchoolContext { get; set; }
    }
}
