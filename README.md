DrivingSchool project

Console functions:
- Instructor: add/delete/list, modify price
- Students: add/delete/list, modify email
- Car: add/delete/list, modify license plate
- Lesson: add/delete/list, modify distance
- List instructors by total payment
- List average speed by sex
- List cars by total distance