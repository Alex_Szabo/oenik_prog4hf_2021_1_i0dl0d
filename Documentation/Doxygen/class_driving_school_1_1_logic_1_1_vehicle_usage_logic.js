var class_driving_school_1_1_logic_1_1_vehicle_usage_logic =
[
    [ "VehicleUsageLogic", "class_driving_school_1_1_logic_1_1_vehicle_usage_logic.html#a77c11abbbb0c8378f729da13ea8fa387", null ],
    [ "AddCar", "class_driving_school_1_1_logic_1_1_vehicle_usage_logic.html#aabff8ffcfb474c6c3622b6ee3015d43a", null ],
    [ "ChangeLicensePlate", "class_driving_school_1_1_logic_1_1_vehicle_usage_logic.html#a95b476a84952dc4a688a6cd5e23ea401", null ],
    [ "GetAllCars", "class_driving_school_1_1_logic_1_1_vehicle_usage_logic.html#ae7a0c3e0f11525602896c00e556f5c55", null ],
    [ "GetCarId", "class_driving_school_1_1_logic_1_1_vehicle_usage_logic.html#af0a4294502563f1c4d710d5c8e07b9a4", null ],
    [ "RemoveCar", "class_driving_school_1_1_logic_1_1_vehicle_usage_logic.html#ae660c03ed034cb91855acde0b453fb8b", null ],
    [ "TotalDistances", "class_driving_school_1_1_logic_1_1_vehicle_usage_logic.html#a33f259e74d0cf46039f7d11730abde31", null ],
    [ "TotalDistancesTask", "class_driving_school_1_1_logic_1_1_vehicle_usage_logic.html#a05696f050f7769cc905e9489e17640e9", null ]
];