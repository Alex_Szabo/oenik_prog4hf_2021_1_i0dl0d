var interface_driving_school_1_1_u_i_1_1_b_l_1_1_i_student_logic =
[
    [ "AddStudent", "interface_driving_school_1_1_u_i_1_1_b_l_1_1_i_student_logic.html#a716da359cdd8da237d30ee814af36fdd", null ],
    [ "EditStudent", "interface_driving_school_1_1_u_i_1_1_b_l_1_1_i_student_logic.html#aa2ee183b86594f11702ca7a7db5dfe54", null ],
    [ "GetAllStudent", "interface_driving_school_1_1_u_i_1_1_b_l_1_1_i_student_logic.html#a5472be5e4dfbfa2409efce8ab3739a93", null ],
    [ "RemoveStudent", "interface_driving_school_1_1_u_i_1_1_b_l_1_1_i_student_logic.html#a3ebfd7631a7f911be3d2c0c62567a391", null ]
];