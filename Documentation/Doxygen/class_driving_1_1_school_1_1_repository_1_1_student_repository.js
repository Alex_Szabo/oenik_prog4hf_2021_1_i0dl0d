var class_driving_1_1_school_1_1_repository_1_1_student_repository =
[
    [ "StudentRepository", "class_driving_1_1_school_1_1_repository_1_1_student_repository.html#a51aa9499887b3f55a0c523f8b0ca7df7", null ],
    [ "ChangeDOB", "class_driving_1_1_school_1_1_repository_1_1_student_repository.html#a8f6148a2a6c98566a0cdea9273319101", null ],
    [ "ChangeEmail", "class_driving_1_1_school_1_1_repository_1_1_student_repository.html#a11c2ff252b044e69853f74132bc88210", null ],
    [ "ChangeFirstName", "class_driving_1_1_school_1_1_repository_1_1_student_repository.html#a4d1a5d427ac11a3368ae0e12e91ac64c", null ],
    [ "ChangeLastName", "class_driving_1_1_school_1_1_repository_1_1_student_repository.html#a20488bfca2ff6da29da75fc3aacfde68", null ],
    [ "ChangePhoneNumber", "class_driving_1_1_school_1_1_repository_1_1_student_repository.html#ac0cbf102dd91fb25816a5fe6c71aac4c", null ],
    [ "ChangeSex", "class_driving_1_1_school_1_1_repository_1_1_student_repository.html#ac7ee1dae4edd92f9a469573c16b12b56", null ],
    [ "GetOne", "class_driving_1_1_school_1_1_repository_1_1_student_repository.html#ad6beb0d642daf8ce654137e4615b9202", null ]
];