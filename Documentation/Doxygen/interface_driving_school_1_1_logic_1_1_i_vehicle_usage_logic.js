var interface_driving_school_1_1_logic_1_1_i_vehicle_usage_logic =
[
    [ "AddCar", "interface_driving_school_1_1_logic_1_1_i_vehicle_usage_logic.html#acdea6e51290c8c223c1698bc685607e6", null ],
    [ "ChangeLicensePlate", "interface_driving_school_1_1_logic_1_1_i_vehicle_usage_logic.html#ab37a9db002fe882933b4b424eae39340", null ],
    [ "GetAllCars", "interface_driving_school_1_1_logic_1_1_i_vehicle_usage_logic.html#a7a01f3f829748816ef12dfd20d9ac071", null ],
    [ "GetCarId", "interface_driving_school_1_1_logic_1_1_i_vehicle_usage_logic.html#ad690a82cfeb88f3bd10ef05130eb3cbd", null ],
    [ "RemoveCar", "interface_driving_school_1_1_logic_1_1_i_vehicle_usage_logic.html#a3eda4b72a73fc827e2ce9bfd6e8999f3", null ],
    [ "TotalDistances", "interface_driving_school_1_1_logic_1_1_i_vehicle_usage_logic.html#ae7e974e6f89f98e754496d4781690601", null ],
    [ "TotalDistancesTask", "interface_driving_school_1_1_logic_1_1_i_vehicle_usage_logic.html#a176552c1c7bfb18e6fe54ce416157cd2", null ]
];