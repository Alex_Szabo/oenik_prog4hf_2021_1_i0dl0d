var interface_driving_school_1_1_logic_1_1_i_staff_logic =
[
    [ "AddInstructor", "interface_driving_school_1_1_logic_1_1_i_staff_logic.html#af1fae9ffcfc3913dfd38c51b0cff7156", null ],
    [ "ChangeInstructorPrice", "interface_driving_school_1_1_logic_1_1_i_staff_logic.html#ace22badd1ab1ffad254fed1c630e7657", null ],
    [ "GetAllInstructors", "interface_driving_school_1_1_logic_1_1_i_staff_logic.html#afbadbf19a366058df4f3be79d4f5952a", null ],
    [ "GetInstructorId", "interface_driving_school_1_1_logic_1_1_i_staff_logic.html#a6fceee30563e58ea94df3635fe35cadb", null ],
    [ "RemoveInstructor", "interface_driving_school_1_1_logic_1_1_i_staff_logic.html#a4a70bc5db0d7954366702bc4f35b1415", null ],
    [ "TotalPayments", "interface_driving_school_1_1_logic_1_1_i_staff_logic.html#a1c773dc93eb2593e07b5b14a18773ade", null ],
    [ "TotalPaymentsTask", "interface_driving_school_1_1_logic_1_1_i_staff_logic.html#ad6800ed70ec7d80db766b2180f4c578e", null ]
];