var interface_driving_1_1_school_1_1_repository_1_1_i_student_repository =
[
    [ "ChangeDOB", "interface_driving_1_1_school_1_1_repository_1_1_i_student_repository.html#a9e32cad0a80eaefc4fc3a15f5f0cca61", null ],
    [ "ChangeEmail", "interface_driving_1_1_school_1_1_repository_1_1_i_student_repository.html#ad73b36a404c08278ca2ee3aaa9ef3b10", null ],
    [ "ChangeFirstName", "interface_driving_1_1_school_1_1_repository_1_1_i_student_repository.html#a478e45cca6821d20844f4cf735d05411", null ],
    [ "ChangeLastName", "interface_driving_1_1_school_1_1_repository_1_1_i_student_repository.html#af0a41cab8f455e28657a90e93bc6049c", null ],
    [ "ChangePhoneNumber", "interface_driving_1_1_school_1_1_repository_1_1_i_student_repository.html#ac612a0d4942d7e7a3c4a2057d0d0ac84", null ],
    [ "ChangeSex", "interface_driving_1_1_school_1_1_repository_1_1_i_student_repository.html#af252dd7f0210cd35ca3f05549bb31192", null ]
];