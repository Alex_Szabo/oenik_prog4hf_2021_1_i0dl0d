var class_driving_school_1_1_u_i_1_1_v_m_1_1_main_view_model =
[
    [ "MainViewModel", "class_driving_school_1_1_u_i_1_1_v_m_1_1_main_view_model.html#a2154540091ef68bc7a2aa8d9ad23688c", null ],
    [ "MainViewModel", "class_driving_school_1_1_u_i_1_1_v_m_1_1_main_view_model.html#a1698df7469a5765fb7b5d0cb0608df42", null ],
    [ "AddCmd", "class_driving_school_1_1_u_i_1_1_v_m_1_1_main_view_model.html#af4e8916ea343430734fa6b75141b19c6", null ],
    [ "EditCmd", "class_driving_school_1_1_u_i_1_1_v_m_1_1_main_view_model.html#a598c1d0d401dc6283d2e4dea6a89d4cf", null ],
    [ "RemoveCmd", "class_driving_school_1_1_u_i_1_1_v_m_1_1_main_view_model.html#a565c8cb2c0931b06a41e39616fbce18c", null ],
    [ "Students", "class_driving_school_1_1_u_i_1_1_v_m_1_1_main_view_model.html#a3ae7f78493a6ea1f806ecb8a9783b04d", null ],
    [ "StudentSelected", "class_driving_school_1_1_u_i_1_1_v_m_1_1_main_view_model.html#a756f00d8d6250e8c493ef71aed56cd17", null ]
];