var class_driving_school_1_1_logic_1_1_students_driving_logic =
[
    [ "StudentsDrivingLogic", "class_driving_school_1_1_logic_1_1_students_driving_logic.html#ac4689bd8343f867cedfc587e29fbc56f", null ],
    [ "AddLesson", "class_driving_school_1_1_logic_1_1_students_driving_logic.html#a7c5f204d644761dda7ac733daa30cf16", null ],
    [ "AddStudent", "class_driving_school_1_1_logic_1_1_students_driving_logic.html#a3106388d2b46f4dd73ebb8d0351a77fa", null ],
    [ "AverageSpeedBySex", "class_driving_school_1_1_logic_1_1_students_driving_logic.html#ab0fb1cb7793808ebb39aabb75a6395ca", null ],
    [ "AverageSpeedBySexTask", "class_driving_school_1_1_logic_1_1_students_driving_logic.html#a2e88d3582a341c350bf307857cefbad8", null ],
    [ "ChangeDistance", "class_driving_school_1_1_logic_1_1_students_driving_logic.html#aff2f38130d097f5ad7b2b8615b62ec64", null ],
    [ "ChangeDOB", "class_driving_school_1_1_logic_1_1_students_driving_logic.html#a78a3944453078898062777e3207927c5", null ],
    [ "ChangeEmail", "class_driving_school_1_1_logic_1_1_students_driving_logic.html#aa4ca3d30fc95aa688a1d4eecc90369e3", null ],
    [ "ChangeFirstName", "class_driving_school_1_1_logic_1_1_students_driving_logic.html#a4a03615ed28615b62b7f52a91a877998", null ],
    [ "ChangeLastName", "class_driving_school_1_1_logic_1_1_students_driving_logic.html#a25635e26700f046e45feed36e6937ab8", null ],
    [ "ChangePhoneNumber", "class_driving_school_1_1_logic_1_1_students_driving_logic.html#a3b87c7def42e792492fb1f42e63401a2", null ],
    [ "ChangeSex", "class_driving_school_1_1_logic_1_1_students_driving_logic.html#ad4d29beb83bab0423d54d27805657bc7", null ],
    [ "GetAllLessons", "class_driving_school_1_1_logic_1_1_students_driving_logic.html#aa87de1e39cddd5e23c1bc0e7ef9738dc", null ],
    [ "GetAllStudents", "class_driving_school_1_1_logic_1_1_students_driving_logic.html#a5706c4f37530e5434d1ee0d6ff714b76", null ],
    [ "GetLessonId", "class_driving_school_1_1_logic_1_1_students_driving_logic.html#aa21ca47b502c048d0aa54854732ab56d", null ],
    [ "GetStudentId", "class_driving_school_1_1_logic_1_1_students_driving_logic.html#a5efd49bd2f1b551317cc5e98fdd9e5ce", null ],
    [ "RemoveLesson", "class_driving_school_1_1_logic_1_1_students_driving_logic.html#a080e53e0b8cbe6ed156026bd0f44ae95", null ],
    [ "RemoveStudent", "class_driving_school_1_1_logic_1_1_students_driving_logic.html#aad40f40f695be037758c44bce222f399", null ]
];