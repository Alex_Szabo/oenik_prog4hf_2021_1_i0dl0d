var hierarchy =
[
    [ "Application", null, [
      [ "DrivingSchool.UI.App", "class_driving_school_1_1_u_i_1_1_app.html", null ]
    ] ],
    [ "Driving.School.Repository.ARepository< Car >", "class_driving_1_1_school_1_1_repository_1_1_a_repository.html", [
      [ "Driving.School.Repository.CarRepository", "class_driving_1_1_school_1_1_repository_1_1_car_repository.html", null ]
    ] ],
    [ "Driving.School.Repository.ARepository< Instructor >", "class_driving_1_1_school_1_1_repository_1_1_a_repository.html", [
      [ "Driving.School.Repository.InstructorRepository", "class_driving_1_1_school_1_1_repository_1_1_instructor_repository.html", null ]
    ] ],
    [ "Driving.School.Repository.ARepository< Lesson >", "class_driving_1_1_school_1_1_repository_1_1_a_repository.html", [
      [ "Driving.School.Repository.LessonRepository", "class_driving_1_1_school_1_1_repository_1_1_lesson_repository.html", null ]
    ] ],
    [ "Driving.School.Repository.ARepository< Student >", "class_driving_1_1_school_1_1_repository_1_1_a_repository.html", [
      [ "Driving.School.Repository.StudentRepository", "class_driving_1_1_school_1_1_repository_1_1_student_repository.html", null ]
    ] ],
    [ "Attribute", null, [
      [ "DrivingSchool.Data.Model.ToStringAttribute", "class_driving_school_1_1_data_1_1_model_1_1_to_string_attribute.html", null ]
    ] ],
    [ "DrivingSchool.Logic.AverageSpeedBySex", "class_driving_school_1_1_logic_1_1_average_speed_by_sex.html", null ],
    [ "DrivingSchool.Data.Model.Car", "class_driving_school_1_1_data_1_1_model_1_1_car.html", null ],
    [ "Controller", null, [
      [ "DrivingSchool.Web.Controllers.StudentController", "class_driving_school_1_1_web_1_1_controllers_1_1_student_controller.html", null ],
      [ "Web.Controllers.HomeController", "class_web_1_1_controllers_1_1_home_controller.html", null ]
    ] ],
    [ "DbContext", null, [
      [ "DrivingSchool.Data.Model.DrivingSchoolContext", "class_driving_school_1_1_data_1_1_model_1_1_driving_school_context.html", null ]
    ] ],
    [ "Web.Models.ErrorViewModel", "class_web_1_1_models_1_1_error_view_model.html", null ],
    [ "DrivingSchool.Program.Factory", "class_driving_school_1_1_program_1_1_factory.html", null ],
    [ "IComponentConnector", null, [
      [ "DrivingSchool.UI.UI.EditorWindow", "class_driving_school_1_1_u_i_1_1_u_i_1_1_editor_window.html", null ]
    ] ],
    [ "DrivingSchool.UI.BL.IEditorService", "interface_driving_school_1_1_u_i_1_1_b_l_1_1_i_editor_service.html", [
      [ "DrivingSchool.UI.UI.EditorViaWindow", "class_driving_school_1_1_u_i_1_1_u_i_1_1_editor_via_window.html", null ]
    ] ],
    [ "DrivingSchool.Data.Model.Instructor", "class_driving_school_1_1_data_1_1_model_1_1_instructor.html", null ],
    [ "InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "Driving.School.Repository.IRepository< T >", "interface_driving_1_1_school_1_1_repository_1_1_i_repository.html", [
      [ "Driving.School.Repository.ARepository< T >", "class_driving_1_1_school_1_1_repository_1_1_a_repository.html", null ]
    ] ],
    [ "Driving.School.Repository.IRepository< Car >", "interface_driving_1_1_school_1_1_repository_1_1_i_repository.html", [
      [ "Driving.School.Repository.ICarRepository", "interface_driving_1_1_school_1_1_repository_1_1_i_car_repository.html", [
        [ "Driving.School.Repository.CarRepository", "class_driving_1_1_school_1_1_repository_1_1_car_repository.html", null ]
      ] ]
    ] ],
    [ "Driving.School.Repository.IRepository< Instructor >", "interface_driving_1_1_school_1_1_repository_1_1_i_repository.html", [
      [ "Driving.School.Repository.IInstructorRepository", "interface_driving_1_1_school_1_1_repository_1_1_i_instructor_repository.html", [
        [ "Driving.School.Repository.InstructorRepository", "class_driving_1_1_school_1_1_repository_1_1_instructor_repository.html", null ]
      ] ]
    ] ],
    [ "Driving.School.Repository.IRepository< Lesson >", "interface_driving_1_1_school_1_1_repository_1_1_i_repository.html", [
      [ "Driving.School.Repository.ILessonRepository", "interface_driving_1_1_school_1_1_repository_1_1_i_lesson_repository.html", [
        [ "Driving.School.Repository.LessonRepository", "class_driving_1_1_school_1_1_repository_1_1_lesson_repository.html", null ]
      ] ]
    ] ],
    [ "Driving.School.Repository.IRepository< Student >", "interface_driving_1_1_school_1_1_repository_1_1_i_repository.html", [
      [ "Driving.School.Repository.IStudentRepository", "interface_driving_1_1_school_1_1_repository_1_1_i_student_repository.html", [
        [ "Driving.School.Repository.StudentRepository", "class_driving_1_1_school_1_1_repository_1_1_student_repository.html", null ]
      ] ]
    ] ],
    [ "IServiceLocator", null, [
      [ "DrivingSchool.UI.MyIoc", "class_driving_school_1_1_u_i_1_1_my_ioc.html", null ]
    ] ],
    [ "DrivingSchool.Logic.IStaffLogic", "interface_driving_school_1_1_logic_1_1_i_staff_logic.html", [
      [ "DrivingSchool.Logic.StaffLogic", "class_driving_school_1_1_logic_1_1_staff_logic.html", null ]
    ] ],
    [ "DrivingSchool.UI.BL.IStudentLogic", "interface_driving_school_1_1_u_i_1_1_b_l_1_1_i_student_logic.html", [
      [ "DrivingSchool.UI.BL.StudentLogic", "class_driving_school_1_1_u_i_1_1_b_l_1_1_student_logic.html", null ]
    ] ],
    [ "DrivingSchool.Logic.IStudentsDrivingLogic", "interface_driving_school_1_1_logic_1_1_i_students_driving_logic.html", [
      [ "DrivingSchool.Logic.StudentsDrivingLogic", "class_driving_school_1_1_logic_1_1_students_driving_logic.html", null ]
    ] ],
    [ "IValueConverter", null, [
      [ "DrivingSchool.UI.UI.FemaleToRadioButtonConverter", "class_driving_school_1_1_u_i_1_1_u_i_1_1_female_to_radio_button_converter.html", null ],
      [ "DrivingSchool.UI.UI.MaleToRadioButtonConverter", "class_driving_school_1_1_u_i_1_1_u_i_1_1_male_to_radio_button_converter.html", null ]
    ] ],
    [ "DrivingSchool.Logic.IVehicleUsageLogic", "interface_driving_school_1_1_logic_1_1_i_vehicle_usage_logic.html", [
      [ "DrivingSchool.Logic.VehicleUsageLogic", "class_driving_school_1_1_logic_1_1_vehicle_usage_logic.html", null ]
    ] ],
    [ "DrivingSchool.Data.Model.Lesson", "class_driving_school_1_1_data_1_1_model_1_1_lesson.html", null ],
    [ "DrivingSchool.Web.Models.MapperFactory", "class_driving_school_1_1_web_1_1_models_1_1_mapper_factory.html", null ],
    [ "DrivingSchool.Program.Menu", "class_driving_school_1_1_program_1_1_menu.html", null ],
    [ "ObservableObject", null, [
      [ "DrivingSchool.UI.Data.Student", "class_driving_school_1_1_u_i_1_1_data_1_1_student.html", null ]
    ] ],
    [ "Web.Program", "class_web_1_1_program.html", null ],
    [ "RazorPage", null, [
      [ "AspNetCore.Views__ViewImports", "class_asp_net_core_1_1_views_____view_imports.html", null ],
      [ "AspNetCore.Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", null ],
      [ "AspNetCore.Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", null ],
      [ "AspNetCore.Views_Home_Privacy", "class_asp_net_core_1_1_views___home___privacy.html", null ],
      [ "AspNetCore.Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", null ],
      [ "AspNetCore.Views_Shared__ValidationScriptsPartial", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html", null ],
      [ "AspNetCore.Views_Shared_Error", "class_asp_net_core_1_1_views___shared___error.html", null ],
      [ "AspNetCore.Views_Student_StudentDetails", "class_asp_net_core_1_1_views___student___student_details.html", null ],
      [ "AspNetCore.Views_Student_StudentEdit", "class_asp_net_core_1_1_views___student___student_edit.html", null ],
      [ "AspNetCore.Views_Student_StudentIndex", "class_asp_net_core_1_1_views___student___student_index.html", null ]
    ] ],
    [ "RazorPage< IEnumerable< DrivingSchool.Web.Models.Student >>", null, [
      [ "AspNetCore.Views_Student_StudentList", "class_asp_net_core_1_1_views___student___student_list.html", null ]
    ] ],
    [ "SimpleIoc", null, [
      [ "DrivingSchool.UI.MyIoc", "class_driving_school_1_1_u_i_1_1_my_ioc.html", null ]
    ] ],
    [ "DrivingSchool.Logic.Tests.StaffLogicTest", "class_driving_school_1_1_logic_1_1_tests_1_1_staff_logic_test.html", null ],
    [ "Web.Startup", "class_web_1_1_startup.html", null ],
    [ "DrivingSchool.Web.Models.Student", "class_driving_school_1_1_web_1_1_models_1_1_student.html", null ],
    [ "DrivingSchool.Data.Model.Student", "class_driving_school_1_1_data_1_1_model_1_1_student.html", null ],
    [ "DrivingSchool.Web.Models.StudentListViewModel", "class_driving_school_1_1_web_1_1_models_1_1_student_list_view_model.html", null ],
    [ "DrivingSchool.Logic.Tests.StudentsDrivingLogicTest", "class_driving_school_1_1_logic_1_1_tests_1_1_students_driving_logic_test.html", null ],
    [ "DrivingSchool.Logic.TotalDistances", "class_driving_school_1_1_logic_1_1_total_distances.html", null ],
    [ "DrivingSchool.Logic.TotalPayments", "class_driving_school_1_1_logic_1_1_total_payments.html", null ],
    [ "DrivingSchool.Logic.Tests.VehicleUsageLogicTest", "class_driving_school_1_1_logic_1_1_tests_1_1_vehicle_usage_logic_test.html", null ],
    [ "ViewModelBase", null, [
      [ "DrivingSchool.UI.VM.EditorViewModel", "class_driving_school_1_1_u_i_1_1_v_m_1_1_editor_view_model.html", null ],
      [ "DrivingSchool.UI.VM.MainViewModel", "class_driving_school_1_1_u_i_1_1_v_m_1_1_main_view_model.html", null ]
    ] ],
    [ "Window", null, [
      [ "DrivingSchool.UI.MainWindow", "class_driving_school_1_1_u_i_1_1_main_window.html", null ]
    ] ],
    [ "Window", null, [
      [ "DrivingSchool.UI.UI.EditorWindow", "class_driving_school_1_1_u_i_1_1_u_i_1_1_editor_window.html", null ]
    ] ]
];