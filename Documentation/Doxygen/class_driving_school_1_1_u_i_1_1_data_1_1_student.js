var class_driving_school_1_1_u_i_1_1_data_1_1_student =
[
    [ "CopyFrom", "class_driving_school_1_1_u_i_1_1_data_1_1_student.html#a51b44e43d38ac8234a4ad9e2d4de67f5", null ],
    [ "DateOfBirth", "class_driving_school_1_1_u_i_1_1_data_1_1_student.html#ab22a51796eb681abc4c95d3a81575378", null ],
    [ "Email", "class_driving_school_1_1_u_i_1_1_data_1_1_student.html#ac2aa716a654d8131779dcecb3183d5e0", null ],
    [ "FirstName", "class_driving_school_1_1_u_i_1_1_data_1_1_student.html#a12d99ed3fd8305469cae0015c8f81edd", null ],
    [ "Id", "class_driving_school_1_1_u_i_1_1_data_1_1_student.html#af9b1a5efd8c5eb37731d2927d35d86c4", null ],
    [ "LastName", "class_driving_school_1_1_u_i_1_1_data_1_1_student.html#a276b533df9e0e9a2f6872a5498171d5a", null ],
    [ "PhoneNumber", "class_driving_school_1_1_u_i_1_1_data_1_1_student.html#a9ab04c783663582b28ffd0fbe7d6d9f5", null ],
    [ "Sex", "class_driving_school_1_1_u_i_1_1_data_1_1_student.html#a487cc8ad38dcc52adce73b16931ff093", null ]
];