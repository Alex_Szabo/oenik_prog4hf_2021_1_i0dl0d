var class_driving_school_1_1_data_1_1_model_1_1_student =
[
    [ "Student", "class_driving_school_1_1_data_1_1_model_1_1_student.html#a1543674b5014763436aa7646b3d5e1d1", null ],
    [ "ToString", "class_driving_school_1_1_data_1_1_model_1_1_student.html#a48bcf94aaed3621c912689fc87b398c3", null ],
    [ "DateOfBirth", "class_driving_school_1_1_data_1_1_model_1_1_student.html#a17c882190c73520c1c71b9eb7900c934", null ],
    [ "Email", "class_driving_school_1_1_data_1_1_model_1_1_student.html#aeaa814dce1c8fd92209efd10831dda93", null ],
    [ "FirstName", "class_driving_school_1_1_data_1_1_model_1_1_student.html#a29e911fa3e41cf75f1cf2312361c1e0b", null ],
    [ "LastName", "class_driving_school_1_1_data_1_1_model_1_1_student.html#ac49c352d74e6a32e681a6b68da5144d8", null ],
    [ "Lesson", "class_driving_school_1_1_data_1_1_model_1_1_student.html#a78c90d4c4bd5342f15f3d590121d42c3", null ],
    [ "PhoneNumber", "class_driving_school_1_1_data_1_1_model_1_1_student.html#ab610c36c04892bc4ac5d40ab04c9ba12", null ],
    [ "Sex", "class_driving_school_1_1_data_1_1_model_1_1_student.html#af44ee7d71f37539d6d450f53db8dc690", null ],
    [ "StudentId", "class_driving_school_1_1_data_1_1_model_1_1_student.html#ae7919d0f2e76d4d8e35a067dceae65e0", null ]
];