var class_driving_1_1_school_1_1_repository_1_1_a_repository =
[
    [ "ARepository", "class_driving_1_1_school_1_1_repository_1_1_a_repository.html#a99c854024ade1345b61582d1118d6367", null ],
    [ "Add", "class_driving_1_1_school_1_1_repository_1_1_a_repository.html#a8c0c60306d8875ef7da5b32ff98d8bd2", null ],
    [ "GetAll", "class_driving_1_1_school_1_1_repository_1_1_a_repository.html#afea79f6a3eaa5353d3e6e5dcb40392a9", null ],
    [ "GetOne", "class_driving_1_1_school_1_1_repository_1_1_a_repository.html#a551cfc779cab2b823b1c20c1dea74c02", null ],
    [ "Remove", "class_driving_1_1_school_1_1_repository_1_1_a_repository.html#a88e0145faa87c5edc056a0bb273494a6", null ],
    [ "Ctx", "class_driving_1_1_school_1_1_repository_1_1_a_repository.html#a63b1ef3a0511cfe9167248ac1ef0eefc", null ]
];