var class_driving_school_1_1_data_1_1_model_1_1_car =
[
    [ "Car", "class_driving_school_1_1_data_1_1_model_1_1_car.html#a853c6e72e6e83518fcebb146ca425f50", null ],
    [ "Equals", "class_driving_school_1_1_data_1_1_model_1_1_car.html#a0e68ca25f4fa69ac6fa374a966e17679", null ],
    [ "GetHashCode", "class_driving_school_1_1_data_1_1_model_1_1_car.html#a71ac85c380318639e5bebbc57a8a2b3d", null ],
    [ "ToString", "class_driving_school_1_1_data_1_1_model_1_1_car.html#a409e39fbe0a906973234050f4be397d4", null ],
    [ "Automatic", "class_driving_school_1_1_data_1_1_model_1_1_car.html#a2d58c7221b471d63dd8faf9a43c11930", null ],
    [ "Brand", "class_driving_school_1_1_data_1_1_model_1_1_car.html#ac64918c7fa8557005ab0e716ed7bdae9", null ],
    [ "CarId", "class_driving_school_1_1_data_1_1_model_1_1_car.html#a698059ba901165c2c9b0005c5a526507", null ],
    [ "Lesson", "class_driving_school_1_1_data_1_1_model_1_1_car.html#a55a95cf96765aa8eac8aa47bfece946f", null ],
    [ "LicensePlate", "class_driving_school_1_1_data_1_1_model_1_1_car.html#ab7530c032279ba8b3b65a6aa0099cc69", null ],
    [ "ManufacturingYear", "class_driving_school_1_1_data_1_1_model_1_1_car.html#a1e81de7761966751b22cc885fa5bea8d", null ],
    [ "Type", "class_driving_school_1_1_data_1_1_model_1_1_car.html#a1147da00991f32e6177aa871ea12a1a0", null ]
];