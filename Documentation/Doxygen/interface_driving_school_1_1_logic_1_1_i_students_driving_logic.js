var interface_driving_school_1_1_logic_1_1_i_students_driving_logic =
[
    [ "AddLesson", "interface_driving_school_1_1_logic_1_1_i_students_driving_logic.html#ae15a862627f68b76f6828af7c3bc839b", null ],
    [ "AddStudent", "interface_driving_school_1_1_logic_1_1_i_students_driving_logic.html#a08ca8f5b6ee101ce1bdd1cd955866528", null ],
    [ "AverageSpeedBySex", "interface_driving_school_1_1_logic_1_1_i_students_driving_logic.html#a7a39c0099057b5c5140b59bdad3c33c2", null ],
    [ "AverageSpeedBySexTask", "interface_driving_school_1_1_logic_1_1_i_students_driving_logic.html#a5002003beae5b6985a0156551113e96d", null ],
    [ "ChangeDistance", "interface_driving_school_1_1_logic_1_1_i_students_driving_logic.html#aa120a98e3fbd02b63a6afac4045dd35b", null ],
    [ "ChangeDOB", "interface_driving_school_1_1_logic_1_1_i_students_driving_logic.html#a5f46a0beb23c3163487f3be8019b8fa1", null ],
    [ "ChangeEmail", "interface_driving_school_1_1_logic_1_1_i_students_driving_logic.html#a49ae1de4206199ac7131fa123a1dbadc", null ],
    [ "ChangeFirstName", "interface_driving_school_1_1_logic_1_1_i_students_driving_logic.html#a6ca00f8c052c277767e3c8a1a6c000af", null ],
    [ "ChangeLastName", "interface_driving_school_1_1_logic_1_1_i_students_driving_logic.html#ad7fd6745e7a7e51239734a89e97975ff", null ],
    [ "ChangePhoneNumber", "interface_driving_school_1_1_logic_1_1_i_students_driving_logic.html#a783db38578cae569b12be545de337961", null ],
    [ "ChangeSex", "interface_driving_school_1_1_logic_1_1_i_students_driving_logic.html#a94550d5f21691f8dafc16a30072d72e0", null ],
    [ "GetAllLessons", "interface_driving_school_1_1_logic_1_1_i_students_driving_logic.html#a3fbd3a26014c58cceecb45065c64f87e", null ],
    [ "GetAllStudents", "interface_driving_school_1_1_logic_1_1_i_students_driving_logic.html#a3f1652030cbed71ae8df6e96a20aee98", null ],
    [ "GetLessonId", "interface_driving_school_1_1_logic_1_1_i_students_driving_logic.html#a5db6fa30a74a6b5886312e758a20e5fb", null ],
    [ "GetStudentId", "interface_driving_school_1_1_logic_1_1_i_students_driving_logic.html#ac052857a71b6684e156b0aff5fd286a5", null ],
    [ "RemoveLesson", "interface_driving_school_1_1_logic_1_1_i_students_driving_logic.html#a62b87c11667df86f4a27cacb40993056", null ],
    [ "RemoveStudent", "interface_driving_school_1_1_logic_1_1_i_students_driving_logic.html#a1a30b3845422b4ac088c532d374efb33", null ]
];