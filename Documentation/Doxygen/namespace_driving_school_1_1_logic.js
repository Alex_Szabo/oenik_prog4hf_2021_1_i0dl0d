var namespace_driving_school_1_1_logic =
[
    [ "Tests", "namespace_driving_school_1_1_logic_1_1_tests.html", "namespace_driving_school_1_1_logic_1_1_tests" ],
    [ "AverageSpeedBySex", "class_driving_school_1_1_logic_1_1_average_speed_by_sex.html", "class_driving_school_1_1_logic_1_1_average_speed_by_sex" ],
    [ "IStaffLogic", "interface_driving_school_1_1_logic_1_1_i_staff_logic.html", "interface_driving_school_1_1_logic_1_1_i_staff_logic" ],
    [ "IStudentsDrivingLogic", "interface_driving_school_1_1_logic_1_1_i_students_driving_logic.html", "interface_driving_school_1_1_logic_1_1_i_students_driving_logic" ],
    [ "IVehicleUsageLogic", "interface_driving_school_1_1_logic_1_1_i_vehicle_usage_logic.html", "interface_driving_school_1_1_logic_1_1_i_vehicle_usage_logic" ],
    [ "StaffLogic", "class_driving_school_1_1_logic_1_1_staff_logic.html", "class_driving_school_1_1_logic_1_1_staff_logic" ],
    [ "StudentsDrivingLogic", "class_driving_school_1_1_logic_1_1_students_driving_logic.html", "class_driving_school_1_1_logic_1_1_students_driving_logic" ],
    [ "TotalDistances", "class_driving_school_1_1_logic_1_1_total_distances.html", "class_driving_school_1_1_logic_1_1_total_distances" ],
    [ "TotalPayments", "class_driving_school_1_1_logic_1_1_total_payments.html", "class_driving_school_1_1_logic_1_1_total_payments" ],
    [ "VehicleUsageLogic", "class_driving_school_1_1_logic_1_1_vehicle_usage_logic.html", "class_driving_school_1_1_logic_1_1_vehicle_usage_logic" ]
];