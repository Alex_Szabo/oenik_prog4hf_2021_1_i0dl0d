var class_driving_school_1_1_logic_1_1_staff_logic =
[
    [ "StaffLogic", "class_driving_school_1_1_logic_1_1_staff_logic.html#a427d1de9b3d2ffd14599c90209cc196d", null ],
    [ "AddInstructor", "class_driving_school_1_1_logic_1_1_staff_logic.html#ad4100a7d7abe73b910245b30b2822acf", null ],
    [ "ChangeInstructorPrice", "class_driving_school_1_1_logic_1_1_staff_logic.html#a2fd47799398e6ccad19e5ba06fd20dfd", null ],
    [ "GetAllInstructors", "class_driving_school_1_1_logic_1_1_staff_logic.html#a822769741728dfaa2194a67e67f6037b", null ],
    [ "GetInstructorId", "class_driving_school_1_1_logic_1_1_staff_logic.html#a6226b9b58506218dd2f9032a3d92572f", null ],
    [ "RemoveInstructor", "class_driving_school_1_1_logic_1_1_staff_logic.html#a405b215bb125772758c6708f962ce47e", null ],
    [ "TotalPayments", "class_driving_school_1_1_logic_1_1_staff_logic.html#aea58ce41c8230e971c48698cb3044d8e", null ],
    [ "TotalPaymentsTask", "class_driving_school_1_1_logic_1_1_staff_logic.html#add124054c5703209a4832d139ae18974", null ]
];