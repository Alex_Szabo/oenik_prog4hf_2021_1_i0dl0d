var class_driving_school_1_1_data_1_1_model_1_1_lesson =
[
    [ "ToString", "class_driving_school_1_1_data_1_1_model_1_1_lesson.html#ae04fb1eee758b9ee06afdfa03517d80f", null ],
    [ "Car", "class_driving_school_1_1_data_1_1_model_1_1_lesson.html#a884181d17eae2647da205c6da5a58058", null ],
    [ "CarId", "class_driving_school_1_1_data_1_1_model_1_1_lesson.html#a1356e5e2407322c3c83dc52ed569d780", null ],
    [ "Distance", "class_driving_school_1_1_data_1_1_model_1_1_lesson.html#a26418b35c2025342422ff5c4e540cd2b", null ],
    [ "Instructor", "class_driving_school_1_1_data_1_1_model_1_1_lesson.html#a412134b54008854bc6544d7823559873", null ],
    [ "InstructorId", "class_driving_school_1_1_data_1_1_model_1_1_lesson.html#ac9515b39fe863322dbf39b43e03dd9c5", null ],
    [ "LessonDate", "class_driving_school_1_1_data_1_1_model_1_1_lesson.html#a4d7111268812b5c6df7c70f230bcfeb6", null ],
    [ "LessonId", "class_driving_school_1_1_data_1_1_model_1_1_lesson.html#a8ac777df6ca50ef4a462baa077dac9ba", null ],
    [ "LessonTime", "class_driving_school_1_1_data_1_1_model_1_1_lesson.html#a14d049db9a88d5b5a8b6f8eb61e28042", null ],
    [ "Student", "class_driving_school_1_1_data_1_1_model_1_1_lesson.html#a98bf5b2db275795aed08b2658e9c08ed", null ],
    [ "StudentId", "class_driving_school_1_1_data_1_1_model_1_1_lesson.html#a77b55284412b4b4ece3e81b8da6f2a14", null ]
];