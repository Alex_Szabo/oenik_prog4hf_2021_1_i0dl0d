var class_driving_school_1_1_u_i_1_1_b_l_1_1_student_logic =
[
    [ "StudentLogic", "class_driving_school_1_1_u_i_1_1_b_l_1_1_student_logic.html#af569489c1def2b8a28e04a41c5ba69d0", null ],
    [ "AddStudent", "class_driving_school_1_1_u_i_1_1_b_l_1_1_student_logic.html#af99235e3baa4e5430ec1985eaf2d8e73", null ],
    [ "EditStudent", "class_driving_school_1_1_u_i_1_1_b_l_1_1_student_logic.html#accfc0b5626555a7fd145749ee62345a7", null ],
    [ "GetAllStudent", "class_driving_school_1_1_u_i_1_1_b_l_1_1_student_logic.html#afa2be066276c043efcf35c21fa5acb87", null ],
    [ "RemoveStudent", "class_driving_school_1_1_u_i_1_1_b_l_1_1_student_logic.html#af0a36773a5ab67bcae61fcdf765db9bc", null ]
];