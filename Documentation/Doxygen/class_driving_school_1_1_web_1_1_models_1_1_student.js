var class_driving_school_1_1_web_1_1_models_1_1_student =
[
    [ "DateOfBirth", "class_driving_school_1_1_web_1_1_models_1_1_student.html#ad1bb9263392ebb8ce67b7fa36cd8a837", null ],
    [ "Email", "class_driving_school_1_1_web_1_1_models_1_1_student.html#a9ff31d8885a045372261f0c7efe72e39", null ],
    [ "FirstName", "class_driving_school_1_1_web_1_1_models_1_1_student.html#a83b164c9cb0176f28bf75e992a728c0b", null ],
    [ "LastName", "class_driving_school_1_1_web_1_1_models_1_1_student.html#a482c6bc1ef30c8344ff95dab1f9a4cc5", null ],
    [ "PhoneNumber", "class_driving_school_1_1_web_1_1_models_1_1_student.html#a748d48d881d7fdc82911b19bfd6e48f2", null ],
    [ "Sex", "class_driving_school_1_1_web_1_1_models_1_1_student.html#a355a5db4cc30f4b5fa09b500f084fb7d", null ],
    [ "StudentId", "class_driving_school_1_1_web_1_1_models_1_1_student.html#a71dca027fe9228c957a674f5497aba11", null ]
];