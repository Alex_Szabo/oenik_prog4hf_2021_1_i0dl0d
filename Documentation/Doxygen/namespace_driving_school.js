var namespace_driving_school =
[
    [ "Data", "namespace_driving_school_1_1_data.html", "namespace_driving_school_1_1_data" ],
    [ "Logic", "namespace_driving_school_1_1_logic.html", "namespace_driving_school_1_1_logic" ],
    [ "Program", "namespace_driving_school_1_1_program.html", "namespace_driving_school_1_1_program" ],
    [ "UI", "namespace_driving_school_1_1_u_i.html", "namespace_driving_school_1_1_u_i" ],
    [ "Web", "namespace_driving_school_1_1_web.html", "namespace_driving_school_1_1_web" ]
];