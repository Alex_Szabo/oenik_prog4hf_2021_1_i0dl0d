var class_driving_school_1_1_data_1_1_model_1_1_instructor =
[
    [ "Instructor", "class_driving_school_1_1_data_1_1_model_1_1_instructor.html#ab9dc4d2b0fee8e26296b60c13e436f4d", null ],
    [ "Equals", "class_driving_school_1_1_data_1_1_model_1_1_instructor.html#ad91b106db839e8a03b137a09eeaaf4bf", null ],
    [ "GetHashCode", "class_driving_school_1_1_data_1_1_model_1_1_instructor.html#a9c14d83c1ed9ce480c2b1921047178dc", null ],
    [ "ToString", "class_driving_school_1_1_data_1_1_model_1_1_instructor.html#ab13df4d8818634779b102a216363b0c4", null ],
    [ "DateOfBirth", "class_driving_school_1_1_data_1_1_model_1_1_instructor.html#a222beaacc6b7f6c226f4a303e8961729", null ],
    [ "DateOfJoin", "class_driving_school_1_1_data_1_1_model_1_1_instructor.html#aa9dc4a432759b1a6a17af33637220177", null ],
    [ "DateOfLeft", "class_driving_school_1_1_data_1_1_model_1_1_instructor.html#a6d83f1146478404b175d6854f17d80e0", null ],
    [ "FirstName", "class_driving_school_1_1_data_1_1_model_1_1_instructor.html#aa08f2d0fb4c21695612853faf806ab95", null ],
    [ "InstructorId", "class_driving_school_1_1_data_1_1_model_1_1_instructor.html#ae603e13ebbfae7323ce4bcd538c88b87", null ],
    [ "LastName", "class_driving_school_1_1_data_1_1_model_1_1_instructor.html#a29d052c09e929850ce3e0ed0cb7a4d2b", null ],
    [ "Lesson", "class_driving_school_1_1_data_1_1_model_1_1_instructor.html#adaa9a3d8fbdcd9b6854766f3fbc6f9e0", null ],
    [ "PhoneNumber", "class_driving_school_1_1_data_1_1_model_1_1_instructor.html#a7704d17fd986d6a9fcb8cab210247cee", null ],
    [ "Price", "class_driving_school_1_1_data_1_1_model_1_1_instructor.html#a68f32591c1ba7cb4bdb1bdb1487c2fa8", null ],
    [ "Sex", "class_driving_school_1_1_data_1_1_model_1_1_instructor.html#a4fc3ffd9db21c0281155ef22634e4492", null ]
];